package com.workflow.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.workflow.system.model.Bill;

public interface BillMapper {
	
	public Bill getBillById(@Param("billId") Integer billId) throws Exception;
	
	public Bill getBillByFileName(@Param("fileName") String fileName) throws Exception;
	
	public List<Bill> getAllBills() throws Exception;

	public void saveBill(@Param("bill") Bill bill) throws Exception;

	public void deleteBill(@Param("bill") Bill bill) throws Exception;
	
	public void deleteBillById(@Param("id") Integer billId) throws Exception;
	
	public void updateBillName(@Param("id") Integer billId, @Param("billName") String billName) throws Exception;
	
	public void updateFolderName(@Param("id") Integer billId, @Param("billName") String billName) throws Exception;
	
	public void deleteBillByContractorId(@Param("contractorId") Integer contractorId) throws Exception;

	public List<Bill> getBillsByParentId(@Param("parentId") Integer parentId) throws Exception;
	
	public List<Bill> getDirectories() throws Exception;

}
