package com.workflow.system.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.enums.VacationStatus;
import com.workflow.system.mapper.VacationMapper;
import com.workflow.system.model.User;
import com.workflow.system.model.Vacation;

@Service
public class VacationDao implements Serializable, VacationMapper {

	private static final long serialVersionUID = 5830468334314830776L;

	@Autowired
	private VacationMapper mapper;

	@Override
	public Vacation getVacationById(@Param("id") Integer id) throws Exception {
		return mapper.getVacationById(id);
	}

	@Override
	public Vacation getVacationByProcessId(@Param("processId") String processId) throws Exception {
		return mapper.getVacationByProcessId(processId);
	}

	@Override
	public List<Vacation> getVacationsByUser(@Param("user") User user) throws Exception {
		return mapper.getVacationsByUser(user);
	}

	@Override
	public Integer countUserVacations(@Param("user") User user) throws Exception {
		return mapper.countUserVacations(user);
	}

	@Override
	public void saveVacation(@Param("vacation") Vacation vacation) throws Exception {
		mapper.saveVacation(vacation);
	}

	@Override
	public void changeVacationRequestStatus(@Param("id") Integer vacationId, @Param("status") VacationStatus status)
			throws Exception {
		mapper.changeVacationRequestStatus(vacationId, status);
	}

	@Override
	public List<Vacation> getApprovedVacationsByUser(@Param("user") User user) throws Exception {
		return mapper.getApprovedVacationsByUser(user);
	}

	@Override
	public Integer countPendingVacationsByUser(@Param("user") User user) throws Exception{
		return mapper.countPendingVacationsByUser(user);
	}
}