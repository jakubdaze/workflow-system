package com.workflow.system.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.mapper.ContractorMapper;
import com.workflow.system.model.Contractor;

@Service
public class ContractorDao implements Serializable, ContractorMapper {

	private static final long serialVersionUID = -277533612053327291L;

	@Autowired
	private ContractorMapper mapper;

	@Override
	public List<Contractor> getAllContractors() throws Exception {
		return mapper.getAllContractors();
	}

	@Override
	public Contractor getContractorByNip(@Param("nip") String nip) throws Exception {
		return mapper.getContractorByNip(nip);
	}

	@Override
	public Contractor getContractorByEmail(@Param("email") String email) throws Exception {
		return mapper.getContractorByEmail(email);
	}

	@Override
	public Contractor getContractorByPhoneNumber(@Param("phoneNumber") String phoneNumber) throws Exception {
		return mapper.getContractorByPhoneNumber(phoneNumber);
	}

	@Override
	public void saveContractor(@Param("contractor") Contractor contractor) throws Exception {
		mapper.saveContractor(contractor);
	}

	@Override
	public void updateContractor(@Param("contractor") Contractor contractor) throws Exception {
		mapper.updateContractor(contractor);
	}

	@Override
	public void deleteContractor(@Param("contractor") Contractor contractor) throws Exception {
		mapper.deleteContractor(contractor);
	}

	@Override
	public Integer countContractor(@Param("contractor") Contractor contractor) throws Exception {
		return mapper.countContractor(contractor);
	}

	@Override
	public Contractor getContractorById(Integer id) throws Exception {
		return mapper.getContractorById(id);
	}

}
