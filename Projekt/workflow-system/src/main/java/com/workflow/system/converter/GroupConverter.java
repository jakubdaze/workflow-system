package com.workflow.system.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.dao.GroupDao;
import com.workflow.system.model.Group;
import com.workflow.system.model.Permission;

@Component("groupConverter")
@Scope("request")
public class GroupConverter implements Converter {

	@Autowired
	private GroupDao groupDao;

	public GroupConverter() {

	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isNotEmpty(value)) {
			final Integer id = Integer.parseInt(value);
			Group group;
			try {
				group = groupDao.getGroupById(id);
				return group;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			if (!(value instanceof Group)) {
				throw new IllegalArgumentException(
						"value should be: " + Permission.class.getName() + ", but is: " + value.getClass().getName());
			}
			Group group = (Group) value;

			if (group.getGroupId() != null) {
				final Integer id = group.getGroupId();
				return StringUtils.defaultString(id.toString());
			}
		}
		return "";
	}
}