package com.workflow.system.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.mapper.UserMapper;
import com.workflow.system.model.Group;
import com.workflow.system.model.User;
import com.workflow.system.security.AES;

@Service
public class UserDao implements Serializable, UserMapper {

	private static final long serialVersionUID = 1L;

	@Autowired
	private UserMapper userMapper;

	@Override
	public List<User> getAllUsers() throws Exception {
		return userMapper.getAllUsers();
	}

	@Override
	public void saveUser(@Param("user") User user) throws Exception {
		userMapper.saveUser(user);
	}

	@Override
	public void deleteUser(@Param("user") User user) throws Exception {
		userMapper.deleteUser(user);
	}

	@Override
	public void updateUser(@Param("user") User user) throws Exception {
		userMapper.updateUser(user);
	}

	@Override
	public User getUserByEmail(@Param("email") String email) throws Exception {
		return userMapper.getUserByEmail(email);
	}

	@Override
	public void changeUserPassword(@Param("user") User user, @Param("password") String password) throws Exception {
		userMapper.changeUserPassword(user, password);
	}

	@Override
	public User getUserById(@Param("userId") Integer userId) throws Exception {
		return userMapper.getUserById(userId);
	}

	@Override
	public List<User> getUsersByGroup(@Param("group") Group group) throws Exception {
		return userMapper.getUsersByGroup(group);
	}

	@Override
	public Integer countUser(@Param("user") User user) throws Exception {
		return userMapper.countUser(user);
	}

	@Override
	public List<User> getEveryUser() throws Exception {
		return userMapper.getEveryUser();
	}
}
