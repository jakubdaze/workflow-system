package com.workflow.system.converter;

import java.awt.image.BufferedImage;
import java.awt.image.ImagingOpException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;

public class ImageConverter {

	public static String scaleImage(String base64, int width, int height) throws IllegalArgumentException, ImagingOpException, IOException {
		OutputStream resizedImage = new ByteArrayOutputStream();
		InputStream imageInputStream = new ByteArrayInputStream(decodeFromBase64(base64));
		BufferedImage image = ImageIO.read(imageInputStream);
		ImageIO.write(Scalr.resize(image, Method.ULTRA_QUALITY, width, height, Scalr.OP_ANTIALIAS), "JPG", resizedImage);
		base64 = encodeToBase64(((ByteArrayOutputStream) resizedImage).toByteArray());
		resizedImage.close();
		return base64;
	}

	private static byte[] decodeFromBase64(String base64) {
		return Base64.getDecoder().decode(base64.getBytes());
	}

	private static String encodeToBase64(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}

}
