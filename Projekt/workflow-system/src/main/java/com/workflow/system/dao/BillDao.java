package com.workflow.system.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.mapper.BillMapper;
import com.workflow.system.mapper.UserMapper;
import com.workflow.system.model.Bill;
import com.workflow.system.model.User;

@Service
public class BillDao implements Serializable, BillMapper {

	private static final long serialVersionUID = -5086589602966480908L;
	
	@Autowired
	private BillMapper billMapper;

	@Override
	public Bill getBillById(Integer billId) throws Exception {
		return billMapper.getBillById(billId);
	}

	@Override
	public List<Bill> getAllBills() throws Exception {
		return billMapper.getAllBills();
	}

	@Override
	public void saveBill(Bill bill) throws Exception {
		billMapper.saveBill(bill);
	}

	@Override
	public void deleteBill(Bill bill) throws Exception {
		billMapper.deleteBill(bill);
	}
	
	@Override
	public void deleteBillByContractorId(Integer contractorId) throws Exception {
		billMapper.deleteBillByContractorId(contractorId);
	}

	@Override
	public List<Bill> getBillsByParentId(Integer parentId) throws Exception {
		return billMapper.getBillsByParentId(parentId);
	}

	@Override
	public List<Bill> getDirectories() throws Exception {
		return billMapper.getDirectories();
	}

	@Override
	public Bill getBillByFileName(String fileName) throws Exception {
		return billMapper.getBillByFileName(fileName);
	}

	@Override
	public void deleteBillById(Integer billId) throws Exception {
		billMapper.deleteBillById(billId);
	}

	@Override
	public void updateBillName(Integer billId, String billName) throws Exception {
		billMapper.updateBillName(billId, billName);
	}

	@Override
	public void updateFolderName(Integer billId, String billName) throws Exception {
		billMapper.updateFolderName(billId, billName);
	}
}
