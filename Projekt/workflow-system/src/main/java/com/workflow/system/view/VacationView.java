package com.workflow.system.view;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.activiti.services.VacationService;
import com.workflow.system.dao.UserDao;
import com.workflow.system.dao.VacationDao;
import com.workflow.system.enums.VacationStatus;
import com.workflow.system.model.User;
import com.workflow.system.model.Vacation;
import com.workflow.system.security.AESService;

@Component
@Scope("session")
@Qualifier("vacationView")
public class VacationView extends AbstractView implements Serializable {

	private static final long serialVersionUID = 7916579160796392595L;

	@Autowired
	private VacationDao vacationDao;

	@Autowired
	private VacationService vacationService;

	@Autowired
	private LoginView loginView;

	@Autowired
	private UserDao userDao;

	private Vacation vacation;

	private List<Vacation> myVacations;

	public VacationView() {
	}

	private void clear() {
		vacation = null;
	}

	private Integer getNewVacationDayDifference() {
		DateTime startDate = new DateTime(vacation.getStartDate());
		DateTime endDate = new DateTime(vacation.getEndDate());
		return Days.daysBetween(startDate, endDate).getDays();
	}

	public Integer getUserVacationDays(User user) throws Exception {
		user = userDao.getUserById(user.getUserId());
		int year = DateTime.now().getYear();
		int days = 0;
		List<Vacation> vacations = vacationDao.getApprovedVacationsByUser(user);
		for (Vacation vacation : vacations) {
			DateTime startDate = new DateTime(vacation.getStartDate());
			DateTime endDate = new DateTime(vacation.getEndDate());
			if (startDate.getYear() == year && endDate.getYear() == year) {
				int difference = Days.daysBetween(startDate, endDate).getDays();
				days += difference;
			}
		}
		return days;
	}

	public String addVacation() {
		vacation = new Vacation();
		return "add_vacation";
	}

	public void detailGroup(Vacation vacation) {
		vacation.setUser(AESService.decrypt(vacation.getUser()));
		this.vacation = vacation;
	}

	public String cancel() {
		return "cancel";
	}

	public List<Vacation> getMyVacations() {
		try {
			myVacations = vacationDao.getVacationsByUser(loginView.getLoggedUser());
		} catch (Exception e) {
			addGrowl("vacation.getall.fail");
		}
		return myVacations;
	}

	public String saveVacation() {
		try {
			vacation.setUser(loginView.getLoggedUser());
			Integer vacationDays = getUserVacationDays(vacation.getUser());
			Integer amountOfPendingRequest = vacationDao.countPendingVacationsByUser(vacation.getUser());
			if(amountOfPendingRequest > 0){
				addGrowl("vacation.pending");
				return "vacations";
			}
			if (vacationDays > 24 || vacationDays + getNewVacationDayDifference() > 24) {
				addGrowl("vacation.day.limit", new Integer(24 - vacationDays).toString());
				return "vacations";
			}
			String processId = vacationService.addVacationRequest(vacation);
			if (processId != null && !processId.isEmpty()) {
				vacation.setStatus(VacationStatus.PENDING);
				vacation.setProcessId(processId);
				vacationDao.saveVacation(vacation);
				addGrowl("vacation.add.success");
			}
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("vacation.add.fail");
		}
		clear();
		return "vacations";
	}

	public Date getMinEndDate() {
		DateTime tommorow = new DateTime().withTimeAtStartOfDay().plusDays(1);
		return tommorow.toDate();
	}

	public Date getMaxEndDate() {
		int year = DateTime.now().getYear();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		try {
			Date date = sdf.parse("31-12-" + year);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public VacationDao getVacationDao() {
		return vacationDao;
	}

	public void setVacationDao(VacationDao vacationDao) {
		this.vacationDao = vacationDao;
	}

	public Vacation getVacation() {
		return vacation;
	}

	public void setVacation(Vacation vacation) {
		this.vacation = vacation;
	}

	public VacationService getVacationService() {
		return vacationService;
	}

	public void setVacationService(VacationService vacationService) {
		this.vacationService = vacationService;
	}
}