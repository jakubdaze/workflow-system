package com.workflow.system.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.workflow.system.enums.InvoiceEnum;
import com.workflow.system.enums.InvoiceStatus;

@Entity
@Table(name = "invoice")
public class Invoice implements Serializable {

	private static final long serialVersionUID = -6516919751189680689L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "invoice_id")
	private Integer invoiceId;
	
	@Column(name = "invoice_number")
	private String invoiceNumber;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "invoice_type")
	private InvoiceEnum invoiceType;

	@Column(name = "issue_date")
	private Date issueDate;
	
	@Column(name = "payment_date")
	private Date paymentDate;
	
	@Column(name = "gross_amount")
	private String grossAmount;
	
	@Column(name = "net_amount")
	private String netAmount;
	
	@Column(name = "invoice_lines")
	private String lines;
	
	@Column(name = "accounting_lines")
	private String accountingLines;
	
	@Column(name = "comment")
	private String comment;

	@Column(name = "process_id")
	private String processId;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private InvoiceStatus status;

	@ManyToOne
	private Bill bill;

	@ManyToOne
	private User user;

	public Invoice() {

	}

	public Invoice(String invoiceNumber, InvoiceEnum invoiceType, Date issueDate, Date paymentDate, String grossAmount,
			String netAmount, String lines, String accountingLines, String comment, Bill bill, User user) {
		super();
		this.invoiceNumber = invoiceNumber;
		this.invoiceType = invoiceType;
		this.issueDate = issueDate;
		this.paymentDate = paymentDate;
		this.grossAmount = grossAmount;
		this.netAmount = netAmount;
		this.lines = lines;
		this.accountingLines = accountingLines;
		this.comment = comment;
		this.bill = bill;
		this.user = user;
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public InvoiceEnum getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(InvoiceEnum invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(String grossAmount) {
		this.grossAmount = grossAmount;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getLines() {
		return lines;
	}

	public void setLines(String lines) {
		this.lines = lines;
	}

	public String getAccountingLines() {
		return accountingLines;
	}

	public void setAccountingLines(String accountingLines) {
		this.accountingLines = accountingLines;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public InvoiceStatus getStatus() {
		return status;
	}

	public void setStatus(InvoiceStatus status) {
		this.status = status;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
