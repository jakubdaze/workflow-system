package com.workflow.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.workflow.system.enums.PermissionEnum;
import com.workflow.system.model.Group;
import com.workflow.system.model.Permission;

public interface GroupMapper {

	public Group getGroupById(@Param("id") Integer id) throws Exception;

	public Group getGroupByName(@Param("group") Group group) throws Exception;

	public List<Group> getGroupByPermissions(@Param("permissionList") List<Permission> permissions) throws Exception;

	public Permission getPermissionById(@Param("id") Integer id) throws Exception;

	public Permission getPermissionByName(@Param("permission") PermissionEnum permission) throws Exception;

	public List<Permission> getGroupPermissions(@Param("group") Group group) throws Exception;

	public List<Group> getAllGroups() throws Exception;

	public void saveGroup(@Param("group") Group group) throws Exception;

	public void updateGroup(@Param("group") Group group) throws Exception;

	public void deleteGroup(@Param("group") Group group) throws Exception;

	public List<Permission> getAllPermissions() throws Exception;

	public void savePermissions(@Param("group") Group group, @Param("permissions") List<Permission> permissions)
			throws Exception;

	public void deleteGroupPermissions(@Param("group") Group group) throws Exception;

	public Integer countGroup(@Param("group") Group group) throws Exception;
}
