package com.workflow.system.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.enums.RequisitionStatus;
import com.workflow.system.enums.VacationStatus;
import com.workflow.system.mapper.RequisitionMapper;
import com.workflow.system.mapper.VacationMapper;
import com.workflow.system.model.Requisition;
import com.workflow.system.model.User;
import com.workflow.system.model.Vacation;

@Service
public class RequisitionDao implements Serializable, RequisitionMapper {

	private static final long serialVersionUID = -7111752019605939416L;
	
	@Autowired
	private RequisitionMapper mapper;

	@Override
	public Requisition getRequisitionById(Integer id) throws Exception {
		return mapper.getRequisitionById(id);
	}

	@Override
	public Requisition getRequisitionByProcessId(String processId) throws Exception {
		return mapper.getRequisitionByProcessId(processId);
	}

	@Override
	public List<Requisition> getRequisitionsByUser(User user) throws Exception {
		return mapper.getRequisitionsByUser(user);
	}

	@Override
	public List<Requisition> getApprovedRequisitionsByUser(User user) throws Exception {
		return mapper.getApprovedRequisitionsByUser(user);
	}

	@Override
	public void changeRequisitionRequestStatus(Integer requisitionId, RequisitionStatus status) throws Exception {
		mapper.changeRequisitionRequestStatus(requisitionId, status);
	}

	@Override
	public void saveRequisition(Requisition requisition) throws Exception {
		mapper.saveRequisition(requisition);
	}

	@Override
	public List<Requisition> getRealizedRequisitionsByUser(User user) throws Exception {
		return mapper.getRealizedRequisitionsByUser(user);
	}

	@Override
	public void changeRequisitionRequestDescribe(Integer requisitionId, Requisition requisition, RequisitionStatus status) throws Exception {
		mapper.changeRequisitionRequestDescribe(requisitionId, requisition, status);
	}

	@Override
	public void changeRequisitionRequestComment(Integer requisitionId, RequisitionStatus status, String comment) throws Exception {
		mapper.changeRequisitionRequestComment(requisitionId, status, comment);
	}
}