package com.workflow.system.view;

import java.awt.image.ImagingOpException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.codec.binary.Base64;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.converter.ImageConverter;
import com.workflow.system.dao.UserDao;
import com.workflow.system.dao.VacationDao;
import com.workflow.system.enums.PermissionEnum;
import com.workflow.system.model.Permission;
import com.workflow.system.model.User;
import com.workflow.system.model.Vacation;
import com.workflow.system.permissions.PermUtil;
import com.workflow.system.security.AES;
import com.workflow.system.security.AESService;
import com.workflow.system.security.SHA256;

@Component
@Scope("session")
@Qualifier("loginView")
public class LoginView extends AbstractView implements Serializable {

	private static final long serialVersionUID = 7916579160796392595L;

	@Autowired
	private UserDao userDao;
	@Autowired
	private PermUtil permUtil;
	@Autowired
	private VacationDao vacationDao;
	private User loggedUser;
	private User editUser;
	private String email;
	private String password;
	private String oldPassword;
	private String newPassword;
	private StreamedContent avatar;
	private StreamedContent dialogAvatar;
	private UploadedFile file;

	private UploadedFile certFile;
	private String certFileContent;

	public LoginView() {
	}

	private void setAvatarImage(int width, int height, boolean dialog) throws Exception {
		User user = userDao.getUserByEmail(AES.encrypt(loggedUser.getEmail()));
		loggedUser = AESService.decrypt(user);
		if (loggedUser.getAvatar() != null) {
			String avatarToScale = ImageConverter.scaleImage(loggedUser.getAvatar(), width, height);
			InputStream stream = new ByteArrayInputStream(Base64.decodeBase64(avatarToScale));
			if (!dialog) {
				avatar = new DefaultStreamedContent(stream);
			} else {
				dialogAvatar = new DefaultStreamedContent(stream);
			}
		}
	}

	private boolean isOnVacation(User user) throws Exception {
		List<Vacation> userVacations = vacationDao.getApprovedVacationsByUser(loggedUser);
		Date today = new Date();
		for (Vacation vacation : userVacations) {
			if (vacation.getStartDate().before(today) && vacation.getEndDate().after(today)) {
				return true;
			}
		}
		return false;
	}

	public void loadAvatar() throws Exception {
		setAvatarImage(25, 25, false);
	}

	public String login() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			String encryptedEmail = AES.encrypt(email);
			User user = userDao.getUserByEmail(encryptedEmail);
			if (user != null && user.getPassword().equals(SHA256.hash(password))) {
				loggedUser = AESService.decrypt(user);
				if (isOnVacation(loggedUser)) {
					loggedUser = null;
					addMessage("msg.user.on.vacation", null);
					return "";
				} else {
					context.getExternalContext().getSessionMap().put("email", loggedUser.getEmail());
					setAvatarImage(25, 25, false);
					return "login";
				}
			} else {
				loggedUser = null;
				addMessage("msg.bad.credentails", null);
				return "";
			}
		} catch (Exception e) {
			addMessage("msg.login.error", null);
			e.printStackTrace();
		}
		return "";
	}

	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "logout";
	}

	public void checkAuthentication() throws IOException {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

		if (externalContext.getSessionMap().containsKey("email")) {
			externalContext.redirect(externalContext.getRequestContextPath() + "/logged/main.xhtml");
		}
	}

	public boolean hasPerm(String permissionName) {
		Permission permission = new Permission(null, PermissionEnum.valueOf(permissionName));
		if (permUtil.getUserPermissions(loggedUser).contains(permission)) {
			return true;
		} else {
			return false;
		}
	}

	public String changePassword() {
		if (!loggedUser.getPassword().equals(SHA256.hash(oldPassword))) {
			addMessage("msg.old.password", "changePasswordForm:oldPassword");
		} else {
			try {
				userDao.changeUserPassword(loggedUser, SHA256.hash(newPassword));
				loggedUser = AESService.decrypt(userDao.getUserByEmail(AES.encrypt(loggedUser.getEmail())));
				addGrowl("msg.password.change.success");
				return "homepage";
			} catch (Exception e) {
				addGrowl("msg.password.change.error");
			}
		}
		oldPassword = null;
		newPassword = null;
		return "";
	}

	public String changePasswordView() {
		return "account_change_password";
	}

	public String editAccount() {
		editUser = loggedUser;
		return "edit_account";
	}

	public String updateUser() {
		try {
			if (certFileContent != null) {
				editUser.setCert(certFileContent);
			}
			userDao.updateUser(AESService.encrypt(editUser));
			loggedUser = editUser;
			setAvatarImage(25, 25, false);
			addGrowl("account.update.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("account.update.fail");
		}
		return "homepage";
	}

	public void upload(FileUploadEvent event) throws IllegalArgumentException, ImagingOpException, IOException {
		this.file = event.getFile();
		if (file != null) {
			editUser.setAvatar(ImageConverter.scaleImage(Base64.encodeBase64String(file.getContents()), 80, 80));
		}
	}

	public String deleteAccount() {
		try {
			userDao.deleteUser(loggedUser);
			addGrowl("account.delete.success");
			return logout();
		} catch (Exception e) {
			addGrowl("account.delete.fail");
			return "";
		}
	}

	public void certUpload(FileUploadEvent event) {
		this.certFile = event.getFile();
		this.certFileContent = Base64.encodeBase64String(certFile.getContents());
	}

	public String getLoggedUserEmail() throws Exception {
		return loggedUser.getEmail();
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public User getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(User loggedUser) {
		this.loggedUser = loggedUser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public User getEditUser() {
		return editUser;
	}

	public void setEditUser(User editUser) {
		this.editUser = editUser;
	}

	public PermUtil getPermUtil() {
		return permUtil;
	}

	public void setPermUtil(PermUtil permUtil) {
		this.permUtil = permUtil;
	}

	public StreamedContent getAvatar() {
		return avatar;
	}

	public void setAvatar(StreamedContent avatar) {
		this.avatar = avatar;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public StreamedContent getDialogAvatar() throws Exception {
		setAvatarImage(80, 80, true);
		return dialogAvatar;
	}

	public void setDialogAvatar(StreamedContent dialogAvatar) {
		this.dialogAvatar = dialogAvatar;
	}

	public VacationDao getVacationDao() {
		return vacationDao;
	}

	public void setVacationDao(VacationDao vacationDao) {
		this.vacationDao = vacationDao;
	}

}