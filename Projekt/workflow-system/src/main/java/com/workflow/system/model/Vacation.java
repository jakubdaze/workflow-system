package com.workflow.system.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.workflow.system.enums.VacationStatus;

@Entity
@Table(name = "vacation")
public class Vacation implements Serializable {

	private static final long serialVersionUID = -3523657963671247871L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vacation_id")
	private Integer vacationId;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "motivation")
	private String motivation;

	@Column(name = "process_id")
	private String processId;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private VacationStatus status;

	@ManyToOne
	private User user;

	public Vacation() {

	}

	public Vacation(Date startDate, Date endDate, String motivation, User user) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.motivation = motivation;
		this.user = user;
	}

	public Integer getVacationId() {
		return vacationId;
	}

	public void setVacationId(Integer vacationId) {
		this.vacationId = vacationId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getMotivation() {
		return motivation;
	}

	public void setMotivation(String motivation) {
		this.motivation = motivation;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public VacationStatus getStatus() {
		return status;
	}

	public void setStatus(VacationStatus status) {
		this.status = status;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}
}
