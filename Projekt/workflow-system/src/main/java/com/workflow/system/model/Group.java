package com.workflow.system.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user_group")
public class Group implements Serializable {

	private static final long serialVersionUID = -1585961453434133851L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "group_id")
	private Integer groupId;

	@Column(name = "group_name")
	private String groupName;
	
	@ManyToMany
	private List<Permission> permissions;

	public Group() {

	}

	public Group(Integer groupId, String groupName, List<Permission> permissions) {
		this.groupId = groupId;
		this.groupName = groupName;
		this.permissions = permissions;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer userId) {
		this.groupId = userId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
		result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (groupId == null) {
			if (other.groupId != null)
				return false;
		} else if (!groupId.equals(other.groupId))
			return false;
		if (groupName == null) {
			if (other.groupName != null)
				return false;
		} else if (!groupName.equals(other.groupName))
			return false;
		return true;
	}
	
	
}
