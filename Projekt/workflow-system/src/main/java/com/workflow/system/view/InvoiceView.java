package com.workflow.system.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.activiti.services.InvoiceService;
import com.workflow.system.dao.BillDao;
import com.workflow.system.dao.ContractorDao;
import com.workflow.system.dao.InvoiceDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.enums.InvoiceEnum;
import com.workflow.system.enums.InvoiceStatus;
import com.workflow.system.model.Bill;
import com.workflow.system.model.Contractor;
import com.workflow.system.model.Invoice;
import com.workflow.system.model.Requisition;
import com.workflow.system.model.User;
import com.workflow.system.security.AESService;

@Component
@Scope("session")
@Qualifier("invoiceView")
public class InvoiceView extends AbstractView implements Serializable {

	private static final long serialVersionUID = -6875879299855365836L;

	@Autowired
	private InvoiceDao invoiceDao;
	
	@Autowired
	private BillDao billDao;
	
	@Autowired
	private ContractorDao contractorDao;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private UserDao userDao;

	private Invoice invoice;
	
	private Contractor contractor;

	private List<Invoice> accountedInvoices;
	
	private List<Invoice> proccessedInvoices;
	
	private List<InvoiceEnum> types;
	
	private List<InvoiceStatus> statuses;

	public InvoiceView() {
		types = new ArrayList<InvoiceEnum>(Arrays.asList(InvoiceEnum.values()));
		statuses = new ArrayList<InvoiceStatus>(Arrays.asList(InvoiceStatus.values()));
	}

	private void clear(){
		invoice = null;
	}

	public void detailInvoice(Invoice invoice) {
		this.invoice = invoice;
		try {
			Bill bill = billDao.getBillById(invoice.getBill().getBillId());
			this.contractor = AESService.decrypt(contractorDao.getContractorById(bill.getContractorId()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String cancel() {
		return "cancel";
	}

	public List<Invoice> getAccountedInvoices() {
		try {
			accountedInvoices = invoiceDao.getInvoicesByStatus(InvoiceStatus.ACCOUNTED);
		} catch (Exception e) {
			addGrowl("invoice.getall.fail");
		}
		return accountedInvoices;
	}
	
	public List<Invoice> getProccessedInvoices() {
		try {
			proccessedInvoices = invoiceDao.getInvoicesProccessed();
		} catch (Exception e) {
			addGrowl("invoice.getall.fail");
		}
		return proccessedInvoices;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public InvoiceDao getInvoiceDao() {
		return invoiceDao;
	}

	public void setInvoiceDao(InvoiceDao invoiceDao) {
		this.invoiceDao = invoiceDao;
	}

	public InvoiceService getInvoiceService() {
		return invoiceService;
	}

	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Contractor getContractor() {
		return contractor;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}

	public List<InvoiceEnum> getTypes() {
		return types;
	}

	public void setTypes(List<InvoiceEnum> types) {
		this.types = types;
	}

	public List<InvoiceStatus> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<InvoiceStatus> statuses) {
		this.statuses = statuses;
	}

	
}