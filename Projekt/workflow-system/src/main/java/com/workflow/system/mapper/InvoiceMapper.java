package com.workflow.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.workflow.system.enums.InvoiceStatus;
import com.workflow.system.model.Invoice;
import com.workflow.system.model.Bill;

public interface InvoiceMapper {

	public Invoice getInvoiceById(@Param("id") Integer id) throws Exception;
	
	public Invoice getInvoiceByBillId(@Param("id") Integer id) throws Exception;

	public Invoice getInvoiceByProcessId(@Param("processId") String processId) throws Exception;
	
	public List<Invoice> getInvoicesByStatus(@Param("status") InvoiceStatus status) throws Exception;
	
	public List<Invoice> getInvoicesProccessed() throws Exception;

	public List<Invoice> getInvoicesByBill(@Param("bill") Bill bill) throws Exception;

	public void changeInvoiceStatus(@Param("id") Integer invoiceId, @Param("status") InvoiceStatus status,
			@Param("comment") String comment) throws Exception;

	public void changeInvoiceDelete(@Param("id") Integer invoiceId, @Param("status") InvoiceStatus status)
			throws Exception;

	public void changeInvoiceComment(@Param("id") Integer invoiceId, @Param("comment") String comment) throws Exception;

	public void changeInvoiceRegister(@Param("id") Integer invoiceId, @Param("invoice") Invoice invoice,
			@Param("status") InvoiceStatus status) throws Exception;

	public void changeInvoiceDescribe(@Param("id") Integer invoiceId, @Param("invoice") Invoice invoice,
			@Param("status") InvoiceStatus status) throws Exception;

	public void changeInvoiceAccounting(@Param("id") Integer invoiceId, @Param("invoice") Invoice invoice,
			@Param("status") InvoiceStatus status) throws Exception;

	public void saveInvoice(@Param("invoice") Invoice invoice) throws Exception;
}
