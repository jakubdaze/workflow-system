package com.workflow.system.bundle;

import java.util.Locale;

public class ResourceBundle {

	private static java.util.ResourceBundle messages = java.util.ResourceBundle.getBundle("applicationResources",
			new Locale("pl", "PL"));

	public static String getMessage(String string) {
		return messages.getString(string);
	}
}
