package com.workflow.system.activiti.listeners;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.dao.MessageDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.enums.TerminationType;
import com.workflow.system.model.Message;
import com.workflow.system.model.User;
import com.workflow.system.security.AES;
import com.workflow.system.timer.TerminationTimer;

@Service
public class TerminationListener implements JavaDelegate {

	@Autowired
	private UserDao userDao;
	@Autowired
	private MessageDao messageDao;

	private String prepareMessageContent(String motivation, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String formatedDate = sdf.format(date);
		StringBuilder builder = new StringBuilder();
		builder.append("Uzasadnienie: ");
		builder.append(System.getProperty("line.separator"));
		builder.append(motivation);
		builder.append(System.getProperty("line.separator"));
		builder.append("Konto pracownicze zostanie zamknięte " + formatedDate);
		return builder.toString();
	}

	private Message createDecisionMessage(User user, TerminationType type, String motivation, Date date)
			throws Exception {
		Message message = new Message();
		message.setRead(false);
		message.setReciepent(user);
		message.setContent(prepareMessageContent(motivation, date));
		message.setTitle("Podjęto decyzję o Twoim zwolnieniu");
		message.setSendDate(new Date());
		return message;
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		Date date;
		String email = (String) execution.getVariable("user");
		User user = getUserDao().getUserByEmail(AES.encrypt(email));
		TerminationType type = (TerminationType) execution.getVariable("type");
		String motivation = (String) execution.getVariable("motivation");
		if (type.equals(TerminationType.IMMIDIATE)) {
			date = new DateTime(new Date()).plusMinutes(1).toDate(); //Do testów. Normalnie 1 dzień
			Timer timer = new Timer();
			timer.schedule(new TerminationTimer(user, userDao), date);
			messageDao.saveMessage(createDecisionMessage(user, type, motivation, date));
		} else {
			date = new DateTime(new Date()).plusMinutes(5).toDate(); //Do testów. Normalnie 30 dni
			Timer timer = new Timer();
			timer.schedule(new TerminationTimer(user, userDao), date);
			messageDao.saveMessage(createDecisionMessage(user, type, motivation, date));
		}
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
}
