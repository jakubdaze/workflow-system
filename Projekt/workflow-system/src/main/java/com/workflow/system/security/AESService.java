package com.workflow.system.security;

import java.util.ArrayList;
import java.util.List;

import com.workflow.system.model.Address;
import com.workflow.system.model.Contractor;
import com.workflow.system.model.User;

public class AESService {

	private static Address encrypt(Address address) {
		if (address != null) {
			Address encrypted = new Address();
			encrypted.setCity(AES.encrypt(address.getCity()));
			encrypted.setFlatNumber(AES.encrypt(address.getFlatNumber()));
			encrypted.setHouseNumber(AES.encrypt(address.getHouseNumber()));
			encrypted.setStreet(AES.encrypt(address.getStreet()));
			encrypted.setZipCode(AES.encrypt(address.getZipCode()));
			return encrypted;
		}
		return null;
	}

	private static Address decrypt(Address address) {
		if (address != null) {
			Address decrypted = new Address();
			decrypted.setCity(AES.decrypt(address.getCity()));
			decrypted.setFlatNumber(AES.decrypt(address.getFlatNumber()));
			decrypted.setHouseNumber(AES.decrypt(address.getHouseNumber()));
			decrypted.setStreet(AES.decrypt(address.getStreet()));
			decrypted.setZipCode(AES.decrypt(address.getZipCode()));
			return decrypted;
		}
		return null;
	}

	public static User encrypt(User user) {
		User encrypted = new User();
		encrypted.setAddress(encrypt(user.getAddress()));
		encrypted.setEmail(AES.encrypt(user.getEmail()));
		encrypted.setFirstName(AES.encrypt(user.getFirstName()));
		encrypted.setLastName(AES.encrypt(user.getLastName()));
		encrypted.setPhoneNumber(AES.encrypt(user.getPhoneNumber()));
		encrypted.setAvatar(user.getAvatar());
		encrypted.setBirthDate(user.getBirthDate());
		encrypted.setCert(user.getCert());
		encrypted.setPassword(user.getPassword());
		encrypted.setGroup(user.getGroup());
		encrypted.setUserId(user.getUserId());
		return encrypted;
	}

	public static Contractor encrypt(Contractor contractor) {
		Contractor encrypted = new Contractor();
		encrypted.setAddress(encrypt(contractor.getAddress()));
		encrypted.setEmail(AES.encrypt(contractor.getEmail()));
		encrypted.setKrs(AES.encrypt(contractor.getKrs()));
		encrypted.setName(AES.encrypt(contractor.getName()));
		encrypted.setNip(AES.encrypt(contractor.getNip()));
		encrypted.setRegon(AES.encrypt(contractor.getRegon()));
		encrypted.setPhoneNumber(AES.encrypt(contractor.getPhoneNumber()));
		encrypted.setContractorId(contractor.getContractorId());
		return encrypted;
	}

	public static User decrypt(User user) {
		User decrypted = new User();
		decrypted.setAddress(decrypt(user.getAddress()));
		decrypted.setEmail(AES.decrypt(user.getEmail()));
		decrypted.setFirstName(AES.decrypt(user.getFirstName()));
		decrypted.setLastName(AES.decrypt(user.getLastName()));
		decrypted.setPhoneNumber(AES.decrypt(user.getPhoneNumber()));
		decrypted.setAvatar(user.getAvatar());
		decrypted.setBirthDate(user.getBirthDate());
		decrypted.setCert(user.getCert());
		decrypted.setPassword(user.getPassword());
		decrypted.setGroup(user.getGroup());
		decrypted.setUserId(user.getUserId());
		return decrypted;
	}

	public static Contractor decrypt(Contractor contractor) {
		Contractor decrypted = new Contractor();
		decrypted.setAddress(decrypt(contractor.getAddress()));
		decrypted.setEmail(AES.decrypt(contractor.getEmail()));
		decrypted.setKrs(AES.decrypt(contractor.getKrs()));
		decrypted.setName(AES.decrypt(contractor.getName()));
		decrypted.setNip(AES.decrypt(contractor.getNip()));
		decrypted.setRegon(AES.decrypt(contractor.getRegon()));
		decrypted.setPhoneNumber(AES.decrypt(contractor.getPhoneNumber()));
		decrypted.setContractorId(contractor.getContractorId());
		return decrypted;
	}

	public static List<Contractor> decryptContractors(List<Contractor> contractors) {
		List<Contractor> decrypted = new ArrayList<Contractor>();
		for (Contractor contractor : contractors) {
			decrypted.add(decrypt(contractor));
		}
		return decrypted;
	}

	public static List<User> decryptUsers(List<User> users) {
		List<User> decrypted = new ArrayList<User>();
		for (User user : users) {
			decrypted.add(decrypt(user));
		}
		return decrypted;
	}
}
