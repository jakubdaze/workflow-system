package com.workflow.system.view;

import java.awt.image.ImagingOpException;
import java.io.IOException;
import java.io.Serializable;

import org.apache.commons.codec.binary.Base64;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.activiti.services.JobApplicationService;
import com.workflow.system.dao.ApplicationDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.enums.JobApplicationEnum;
import com.workflow.system.model.Address;
import com.workflow.system.model.Group;
import com.workflow.system.model.JobApplication;
import com.workflow.system.model.User;
import com.workflow.system.security.AES;
import com.workflow.system.security.AESService;
import com.workflow.system.security.SHA256;

@Component
@Scope("session")
@Qualifier("jobApplicationView")
public class JobApplicationView extends AbstractView implements Serializable {

	private static final long serialVersionUID = 2402779507502472949L;

	@Autowired
	private UserDao userDao;

	@Autowired
	private ApplicationDao applicationDao;

	@Autowired
	private JobApplicationService jobApplicationService;

	private User user;
	private JobApplication jobApplication;
	private String cv;

	private void clear() {
		this.user = null;
		this.jobApplication = null;
		this.cv = null;
	}

	private boolean userExists(User user) throws Exception {
		if (userDao.countUser(user) > 0) {
			return true;
		} else {
			return false;
		}
	}

	private boolean saveUser(User user) throws Exception {
		Group group = new Group();
		group.setGroupId(2);
		user.setPassword(SHA256.hash(user.getPassword()));
		user.setGroup(group);
		if (userExists(user)) {
			addGrowl("user.exists");
			return false;
		} else {
			userDao.saveUser(AESService.encrypt(user));
			return true;
		}
	}

	public String applyForJob() {
		Address address = new Address();
		setUser(new User());
		user.setAddress(address);
		setJobApplication(new JobApplication());
		return "add_application";
	}

	public void upload(FileUploadEvent event) throws IllegalArgumentException, ImagingOpException, IOException {
		this.setCv(Base64.encodeBase64String(event.getFile().getContents()));
	}

	public void startEmploymentProcess() {
		try {
			if (saveUser(user)) {
				jobApplication.setCv(cv);
				jobApplication.setUser(userDao.getUserByEmail(AES.encrypt(user.getEmail())));
				String processId = jobApplicationService.startEmploymentProcess(user);
				jobApplication.setProcessId(processId);
				applicationDao.saveApplication(jobApplication);
				addGrowl("msg.job.application.sent");
				clear();
			}
		} catch (Exception e) {
			addGrowl("msg.job.application.fail");
			e.printStackTrace();
		}

	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public ApplicationDao getApplicationDao() {
		return applicationDao;
	}

	public void setApplicationDao(ApplicationDao applicationDao) {
		this.applicationDao = applicationDao;
	}

	public JobApplication getJobApplication() {
		return jobApplication;
	}

	public void setJobApplication(JobApplication jobApplication) {
		this.jobApplication = jobApplication;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public JobApplicationService getJobApplicationService() {
		return jobApplicationService;
	}

	public void setJobApplicationService(JobApplicationService jobApplicationService) {
		this.jobApplicationService = jobApplicationService;
	}
}