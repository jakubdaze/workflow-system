package com.workflow.system.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.enums.PermissionEnum;
import com.workflow.system.mapper.GroupMapper;
import com.workflow.system.model.Group;
import com.workflow.system.model.Permission;

@Service
public class GroupDao implements Serializable, GroupMapper {

	private static final long serialVersionUID = 2266869315546718467L;

	@Autowired
	private GroupMapper mapper;

	@Override
	public Group getGroupById(@Param("id") Integer id) throws Exception {
		return mapper.getGroupById(id);
	}

	@Override
	public List<Group> getAllGroups() throws Exception {
		return mapper.getAllGroups();
	}

	@Override
	public void deleteGroup(@Param("group") Group group) throws Exception {
		mapper.deleteGroup(group);
	}

	@Override
	public void savePermissions(@Param("group") Group group, @Param("permissions") List<Permission> permissions)
			throws Exception {
		mapper.savePermissions(group, permissions);
	}

	@Override
	public void saveGroup(@Param("group") Group group) throws Exception {
		mapper.saveGroup(group);
	}

	@Override
	public void updateGroup(@Param("group") Group group) throws Exception {
		mapper.updateGroup(group);
	}

	@Override
	public void deleteGroupPermissions(@Param("group") Group group) throws Exception {
		mapper.deleteGroupPermissions(group);
	}

	@Override
	public List<Permission> getAllPermissions() throws Exception {
		return mapper.getAllPermissions();
	}

	@Override
	public Permission getPermissionById(@Param("id") Integer id) throws Exception {
		return mapper.getPermissionById(id);
	}

	@Override
	public Group getGroupByName(@Param("group") Group group) throws Exception {
		return mapper.getGroupByName(group);
	}

	@Override
	public List<Permission> getGroupPermissions(@Param("group") Group group) throws Exception {
		return mapper.getGroupPermissions(group);
	}

	@Override
	public List<Group> getGroupByPermissions(@Param("permissionList") List<Permission> permissions) throws Exception {
		return mapper.getGroupByPermissions(permissions);
	}

	@Override
	public Permission getPermissionByName(@Param("permission") PermissionEnum permission) throws Exception {
		return mapper.getPermissionByName(permission);
	}

	@Override
	public Integer countGroup(@Param("group") Group group) throws Exception {
		return mapper.countGroup(group);
	}
}
