package com.workflow.system.enums;

public enum PermissionEnum {

	USER_MANAGEMENT("Zarządzanie pracownikami"), GROUP_MANAGEMENT("Zarządzanie grupami"), 
	CONTRACTOR_MANAGEMENT("Zarządzanie kontrahentami"), BILL_ADD("Dodawanie faktur"), BILL_DELETE("Usuwanie faktur"), 
	BILL_VIEW("Podgląd faktur"), BILL_DOWNLOAD("Pobranie faktur"), VACATION_REVIEW("Weryfikacja urlopu"), 
	BILL_ACCOUNTING("Księgowanie faktur"), DASHBOARD_VIEW("Podgląd głównego panelu"), VACATION("Zgłaszanie urlopów"), 
	TASKS("Zadania"), BILLS("Faktury"), CV_VIEW("Przeglądanie CV"), CALL_WITH_APPLICANT("Rozmowa telefoniczna z aplikantem"), 
	MEETING_WITH_APPLICANT("Spotkanie z aplikantem"), EMPLOYMENT_DECISION("Decyzja o zatrudnieniu"), 
	REQUISITION_ACCEPT("Akceptacja zapotrzebowania"), REQUISITION_REALIZATION("Realizacja zapotrzebowania"),
	REQUISITION("Zapotrzebowania"), INVOICE_DESCRIBE("Opisywanie faktur"), INVOICE_MANAGER("Akceptacja faktur Dyrektorzy"), 
	INVOICE_ADM("Akceptacja faktur zarząd"), INVOICE_MERCHANT("Akceptacja faktur kupiec"), INVOICE_INTERNAL_CONTROL("Kontrola wewnętrzna");
	private String name;

	PermissionEnum(String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
