package com.workflow.system.converter;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.dao.GroupDao;
import com.workflow.system.model.Permission;

@Component("permissionConverter")
@Scope("request")
public class PermissionConverter implements Converter{

	@Autowired
	private GroupDao groupDao;
	
	public PermissionConverter(){
		
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isNotEmpty(value)) {
			final Integer id = Integer.parseInt(value);
			Permission permission;
			try {
				permission = groupDao.getPermissionById(id);
				return permission;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			if (!(value instanceof Permission)) {
				throw new IllegalArgumentException(
						"value should be: " + Permission.class.getName() + ", but is: " + value.getClass().getName());
			}
			Permission permission = (Permission) value;

			if (permission.getPermissionId() != null) {
				final Integer id = permission.getPermissionId();
				return StringUtils.defaultString(id.toString());
			}
		}
		return "";
	}
}