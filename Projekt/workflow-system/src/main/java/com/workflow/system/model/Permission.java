package com.workflow.system.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.workflow.system.enums.PermissionEnum;

@Entity
@Table(name = "permission")
public class Permission implements Serializable {

	private static final long serialVersionUID = 7585146060546977365L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "permission_id")
	private Integer permissionId;

	@Column(name = "permission_name")
	@Enumerated(EnumType.STRING)
	private PermissionEnum permissionName;

	public Permission() {

	}

	public Permission(Integer permissionId, PermissionEnum permissionName) {
		this.permissionId = permissionId;
		this.permissionName = permissionName;
	}

	public Integer getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Integer permissionId) {
		this.permissionId = permissionId;
	}

	public PermissionEnum getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(PermissionEnum permissionName) {
		this.permissionName = permissionName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((permissionName == null) ? 0 : permissionName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permission other = (Permission) obj;
		if (permissionName != other.permissionName)
			return false;
		return true;
	}
}
