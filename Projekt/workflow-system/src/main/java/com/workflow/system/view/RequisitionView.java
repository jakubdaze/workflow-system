package com.workflow.system.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.activiti.services.RequisitionService;
import com.workflow.system.dao.RequisitionDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.enums.RequisitionStatus;
import com.workflow.system.model.Requisition;
import com.workflow.system.model.User;

@Component
@Scope("session")
@Qualifier("requisitionView")
public class RequisitionView extends AbstractView implements Serializable {

	private static final long serialVersionUID = 5676725453900833234L;

	@Autowired
	private RequisitionDao requisitionDao;

	@Autowired
	private RequisitionService requisitionService;

	@Autowired
	private LoginView loginView;

	@Autowired
	private UserDao userDao;

	private Requisition requisition;

	private List<Requisition> myRequisitions;

	public RequisitionView() {
	}

	private void clear() {
		requisition = null;
	}

	public String addRequisition(User user) {
		requisition = new Requisition();
		try {
			requisition.setUser(loginView.getLoggedUser());
			String processId = requisitionService.addRequisitionRequest(user);
			if (processId != null && !processId.isEmpty()) {
				requisition.setStatus(RequisitionStatus.DESCRIBING);
				requisition.setProcessId(processId);
				requisitionDao.saveRequisition(requisition);
				addGrowl("requisition.add.success");
			}
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("requisition.add.fail");
		}
		clear();
		return "tasks";
	}

	public void detailGroup(Requisition requisition) {
		this.requisition = requisition;
	}

	public String cancel() {
		return "cancel";
	}

	public List<Requisition> getMyRequisitions() {
		try {
			myRequisitions = requisitionDao.getRequisitionsByUser(loginView.getLoggedUser());
		} catch (Exception e) {
			addGrowl("vacation.getall.fail");
		}
		return myRequisitions;
	}

	public Date getMinEndDate() {
		DateTime tommorow = new DateTime().withTimeAtStartOfDay().plusDays(1);
		return tommorow.toDate();
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public RequisitionDao getRequisitionDao() {
		return requisitionDao;
	}

	public void setRequisitionDao(RequisitionDao requisitionDao) {
		this.requisitionDao = requisitionDao;
	}

	public RequisitionService getRequisitionService() {
		return requisitionService;
	}

	public void setRequisitionService(RequisitionService requisitionService) {
		this.requisitionService = requisitionService;
	}

	public Requisition getRequisition() {
		return requisition;
	}

	public void setRequisition(Requisition requisition) {
		this.requisition = requisition;
	}

}