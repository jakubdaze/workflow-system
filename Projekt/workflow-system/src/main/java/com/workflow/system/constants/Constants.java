package com.workflow.system.constants;

public class Constants {

	public final static String VACATION_REVIEW_FORM_KEY = "vacationRequestReviewView";

	public final static String CV_REVIEW_FORM_KEY = "reviewCvView";
	public final static String APPLICANT_PHONE_CALL_FORM_KEY = "applicantPhoneCallView";
	public final static String APPLICANT_MEETING_FORM_KEY = "applicantMeetingView";

	public final static String REQUISITION_DESCRIBE_FORM_KEY = "requisitionRequestDescribeView";
	public final static String REQUISITION_ACCEPTANCE_FORM_KEY = "requisitionRequestAcceptanceView";
	public final static String REQUISITION_REALIZATION_FORM_KEY = "requisitionRequestRealizationView";
	
	public final static String INVOICE_REGISTER_FORM_KEY = "invoiceRegisterView";
	public final static String INVOICE_DESCRIBE_FORM_KEY = "invoiceDescribeView";
	public final static String INVOICE_KDACCEPT_FORM_KEY = "invoiceKDAcceptView";
	public final static String INVOICE_KZACCEPT_FORM_KEY = "invoiceKZAcceptView";
	public final static String INVOICE_KKW_FORM_KEY = "invoiceInternalControlView";
	public final static String INVOICE_KADESCRIBE_FORM_KEY = "invoiceKADescribeView";
	public final static String INVOICE_ZKACCEPT_FORM_KEY = "invoiceZKAcceptView";
	public final static String INVOICE_ZDACCEPT_FORM_KEY = "invoiceZDAcceptView";
	public final static String INVOICE_ACCOUNTING_FORM_KEY = "invoiceAccountingView";

	public final static String SALT = "salt";
}
