package com.workflow.system.enums;

public enum InvoiceEnum {
	PURCHASE("Zakupowa"), SALES("Kosztowa");

	private String name;

	InvoiceEnum(String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
