package com.workflow.system.activiti.listeners;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.dao.UserDao;
import com.workflow.system.model.Group;
import com.workflow.system.model.User;
import com.workflow.system.security.AES;
import com.workflow.system.security.AESService;

@Service
public class HireEmployeeListener implements JavaDelegate {

	@Autowired
	private UserDao userDao;

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		Group group = null;
		User user = userDao.getUserByEmail(AES.encrypt((String) execution.getVariable("user")));
		user.setGroup(group);
		userDao.updateUser(user);
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
}
