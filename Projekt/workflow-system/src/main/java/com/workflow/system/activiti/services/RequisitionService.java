package com.workflow.system.activiti.services;

import java.util.Map;

import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.workflow.system.dao.RequisitionDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.enums.RequisitionStatus;
import com.workflow.system.model.Requisition;
import com.workflow.system.model.User;
import com.workflow.system.security.AES;

@Service
public class RequisitionService extends AbstractService {

	@Autowired
	private RequisitionDao requisitionDao;
	@Autowired
	private UserDao userDao;
	private String describer;

	public String addRequisitionRequest(User user) throws Exception {
		Map<String, Object> variables = Maps.newHashMap();
		variables.put("user", user.getEmail());
		variables.put("status", RequisitionStatus.DESCRIBING);
		describer = user.getEmail();
		return runtimeService.startProcessInstanceByKey("requisitionRequestProcess", variables).getId();
	}

	public void changeRequisitionRequestStatus(Task task, RequisitionStatus status) throws Exception {
		Requisition requisition = requisitionDao.getRequisitionByProcessId(task.getProcessInstanceId());
		requisitionDao.changeRequisitionRequestStatus(requisition.getRequisitionId(), status);
		describer = AES.decrypt(requisition.getUser().getEmail());
		taskService.setVariable(task.getId(), "status", status.toString());
		taskService.complete(task.getId());
	}
	
	public void changeRequisitionRequestComment(Task task, RequisitionStatus status, String comment) throws Exception {
		Requisition requisition = requisitionDao.getRequisitionByProcessId(task.getProcessInstanceId());
		requisitionDao.changeRequisitionRequestComment(requisition.getRequisitionId(), status, comment);
		describer = AES.decrypt(userDao.getUserById(requisition.getUser().getUserId()).getEmail());
		taskService.setVariable(task.getId(), "status", status.toString());
		taskService.setVariable(task.getId(), "comment", comment);
		taskService.complete(task.getId());
	}
	
	public void describeRequisitionRequest(Task task, Requisition requisition, RequisitionStatus status) throws Exception {
		Requisition requisition2 = requisitionDao.getRequisitionByProcessId(task.getProcessInstanceId());
		if (requisition != null) {
			requisition.setComment(null);
			requisitionDao.changeRequisitionRequestDescribe(requisition2.getRequisitionId(), requisition, status);
		}
		Map<String, Object> variables = Maps.newHashMap();
		variables.put("user", AES.decrypt(requisition.getUser().getEmail()));
		variables.put("requisition_date", requisition.getRequisitionDate());
		variables.put("items", requisition.getItems());
		variables.put("ammount", requisition.getAmount());
		variables.put("status", RequisitionStatus.DESCRIBED);
		taskService.setVariables(task.getId(), variables);
		taskService.complete(task.getId());
	}

	public String getDescriber() {
		return describer;
	}

	public void setDescriber(String describer) {
		this.describer = describer;
	}
	
}
