package com.workflow.system.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.mapper.MessageMapper;
import com.workflow.system.model.Message;
import com.workflow.system.model.User;

@Service
public class MessageDao implements Serializable, MessageMapper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8443382448320199803L;

	@Autowired
	private MessageMapper mapper;

	@Override
	public Message getMessageById(@Param("messageId") Integer messageId) throws Exception {
		return mapper.getMessageById(messageId);
	}

	@Override
	public List<Message> getMessagesByReciepent(@Param("user") User user) throws Exception {
		return mapper.getMessagesByReciepent(user);
	}

	@Override
	public List<Message> getMessagesBySender(@Param("user") User user) throws Exception {
		return mapper.getMessagesBySender(user);
	}

	@Override
	public void saveMessage(@Param("message") Message message) throws Exception {
		mapper.saveMessage(message);
	}

	@Override
	public void deleteMessage(@Param("message") Message message) throws Exception {
		mapper.deleteMessage(message);
	}

	@Override
	public void updateMessage(@Param("message") Message message) throws Exception {
		mapper.updateMessage(message);
	}

	@Override
	public Integer countUnreadMessagesByUser(@Param("user") User user) throws Exception{
		return mapper.countUnreadMessagesByUser(user);
	}

}
