package com.workflow.system.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.workflow.system.enums.JobApplicationEnum;

@Entity
@Table(name = "job_application")
public class JobApplication {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "application_id")
	private Integer jobApplicationId;
	@ManyToOne
	private User user;
	@Column(name = "cv", columnDefinition = "LONGTEXT")
	private String cv;
	@Column(name = "meeting_date")
	private Date meetingDate;
	@Column(name = "hire_date")
	private Date hireDate;
	@Column(name = "process_id")
	private String processId;
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private JobApplicationEnum status;

	public Integer getJobApplicationId() {
		return jobApplicationId;
	}

	public void setJobApplicationId(Integer jobApplicationId) {
		this.jobApplicationId = jobApplicationId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public JobApplicationEnum getStatus() {
		return status;
	}

	public void setStatus(JobApplicationEnum status) {
		this.status = status;
	}

	public Date getMeetingDate() {
		return meetingDate;
	}

	public void setMeetingDate(Date meetingDate) {
		this.meetingDate = meetingDate;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
}
