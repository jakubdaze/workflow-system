package com.workflow.system.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.dao.MessageDao;
import com.workflow.system.model.Message;

@Component("messageConverter")
@Scope("request")
public class MessageConverter implements Converter {

	@Autowired
	private MessageDao messageDao;

	public MessageConverter() {

	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isNotEmpty(value)) {
			final Integer id = Integer.parseInt(value);
			Message message;
			try {
				message = messageDao.getMessageById(id);
				return message;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			if (!(value instanceof Message)) {
				throw new IllegalArgumentException(
						"value should be: " + Message.class.getName() + ", but is: " + value.getClass().getName());
			}
			Message message = (Message) value;

			if (message.getMessageId() != null) {
				final Integer id = message.getMessageId();
				return StringUtils.defaultString(id.toString());
			}
		}
		return "";
	}
}