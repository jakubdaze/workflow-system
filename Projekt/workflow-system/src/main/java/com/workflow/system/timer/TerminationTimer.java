package com.workflow.system.timer;

import java.util.TimerTask;

import com.workflow.system.dao.UserDao;
import com.workflow.system.model.User;

public class TerminationTimer extends TimerTask {

	private UserDao userDao;
	private User user;


	public TerminationTimer(User user, UserDao userDao) {
		this.user = user;
		this.userDao = userDao;
	}

	@Override
	public void run() {
		try {
			userDao.deleteUser(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
