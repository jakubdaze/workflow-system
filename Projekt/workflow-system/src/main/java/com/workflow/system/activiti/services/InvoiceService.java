package com.workflow.system.activiti.services;

import java.util.Map;

import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.workflow.system.dao.BillDao;
import com.workflow.system.dao.InvoiceDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.enums.InvoiceStatus;
import com.workflow.system.model.Invoice;
import com.workflow.system.security.AES;

@Service
public class InvoiceService extends AbstractService {

	@Autowired
	private InvoiceDao invoiceDao;
	@Autowired
	private BillDao billDao;
	private UserDao userDao;
	private String registrator;

	public String startInvoiceProcess(Invoice invoice) throws Exception {
		Map<String, Object> variables = Maps.newHashMap();
		variables.put("status", InvoiceStatus.REGISTERING);
		variables.put("registratorEmail", invoice.getUser().getEmail());
		registrator = invoice.getUser().getEmail();
		return runtimeService.startProcessInstanceByKey("invoiceProcess", variables).getId();
	}

	public void changeInvoiceStatus(Task task, InvoiceStatus status, String comment) throws Exception {
		Invoice invoice = invoiceDao.getInvoiceByProcessId(task.getProcessInstanceId());
		try {
			registrator = AES.decrypt(userDao.getUserById(invoice.getUser().getUserId()).getEmail());
		} catch (Exception e) {
			e.printStackTrace();
		}
		invoiceDao.changeInvoiceStatus(invoice.getInvoiceId(), status, comment);
		taskService.setVariable(task.getId(), "status", status.toString());
		taskService.setVariable(task.getId(), "comment", comment);
		taskService.complete(task.getId());
	}
	
	public void changeInvoiceDelete(Task task, InvoiceStatus status) throws Exception {
		Invoice invoice = invoiceDao.getInvoiceByProcessId(task.getProcessInstanceId());
		invoiceDao.changeInvoiceDelete(invoice.getInvoiceId(), status);
		taskService.setVariable(task.getId(), "status", status.toString());
		taskService.complete(task.getId());
	}
	
	public void changeInvoiceComment(Task task, String comment) throws Exception {
		Invoice invoice = invoiceDao.getInvoiceByProcessId(task.getProcessInstanceId());
		try {
			registrator = AES.decrypt(userDao.getUserById(invoice.getUser().getUserId()).getEmail());
		} catch (Exception e) {
			e.printStackTrace();
		}
		invoiceDao.changeInvoiceComment(invoice.getInvoiceId(), comment);
		taskService.setVariable(task.getId(), "comment", comment);
		taskService.complete(task.getId());
	}
	
	public void registerInvoice(Task task, Invoice invoice, InvoiceStatus status) throws Exception {
		Invoice invoice2 = invoiceDao.getInvoiceByProcessId(task.getProcessInstanceId());
		if (invoice != null) {
			invoiceDao.changeInvoiceRegister(invoice2.getInvoiceId(), invoice, status);
		}
		Map<String, Object> variables = Maps.newHashMap();
		variables.put("invoiceNumber", invoice.getInvoiceNumber());
		variables.put("invoiceType", invoice.getInvoiceType());
		variables.put("issueDate", invoice.getIssueDate());
		variables.put("paymentDate", invoice.getPaymentDate());
		variables.put("grossAmount", invoice.getGrossAmount());
		variables.put("netAmount", invoice.getNetAmount());
		variables.put("status", InvoiceStatus.REGISTERED);
		taskService.setVariables(task.getId(), variables);
		taskService.complete(task.getId());
	}
	
	public void describeInvoice(Task task, Invoice invoice, InvoiceStatus status) throws Exception {
		Invoice invoice2 = invoiceDao.getInvoiceByProcessId(task.getProcessInstanceId());
		if (invoice != null) {
			invoiceDao.changeInvoiceDescribe(invoice2.getInvoiceId(), invoice, status);
		}
		Map<String, Object> variables = Maps.newHashMap();
		variables.put("lines", invoice.getLines());
		variables.put("invoiceType", invoice.getInvoiceType());
		variables.put("comment", invoice.getComment());
		variables.put("status", InvoiceStatus.DESCRIBED);
		taskService.setVariables(task.getId(), variables);
		taskService.complete(task.getId());
	}
	
	public void accountInvoice(Task task, Invoice invoice, InvoiceStatus status) throws Exception {
		Invoice invoice2 = invoiceDao.getInvoiceByProcessId(task.getProcessInstanceId());
		if (invoice != null) {
			invoiceDao.changeInvoiceAccounting(invoice2.getInvoiceId(), invoice, status);
		}
		Map<String, Object> variables = Maps.newHashMap();
		variables.put("accountingLines", invoice.getAccountingLines());
		variables.put("status", InvoiceStatus.ACCOUNTED);
		taskService.setVariables(task.getId(), variables);
		taskService.complete(task.getId());
	}

	public String getRegistrator() {
		return registrator;
	}

	public void setRegistrator(String registrator) {
		this.registrator = registrator;
	}

}
