package com.workflow.system.activiti.services;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.workflow.system.enums.TerminationType;
import com.workflow.system.model.User;

@Service
public class TerminationService extends AbstractService {

	public String startTerminationProcess(User user, TerminationType type, String motivation) throws Exception {
		Map<String, Object> variables = Maps.newHashMap();
		variables.put("user", user.getEmail());
		variables.put("type", type);
		variables.put("motivation", motivation);
		return runtimeService.startProcessInstanceByKey("terminationProcess", variables).getId();
	}
}
