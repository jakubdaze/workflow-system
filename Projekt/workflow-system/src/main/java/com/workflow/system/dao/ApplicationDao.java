package com.workflow.system.dao;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.enums.JobApplicationEnum;
import com.workflow.system.mapper.ApplicationMapper;
import com.workflow.system.model.JobApplication;
import com.workflow.system.model.User;

@Service
public class ApplicationDao implements Serializable, ApplicationMapper {

	private static final long serialVersionUID = -6723436546496145814L;
	@Autowired
	private ApplicationMapper mapper;

	@Override
	public JobApplication getApplicationById(@Param("id") Integer id) throws Exception {
		return mapper.getApplicationById(id);
	}

	@Override
	public JobApplication getApplicationByUser(@Param("user") User user) throws Exception {
		return mapper.getApplicationByUser(user);
	}

	@Override
	public JobApplication getApplicationByProcessId(@Param("processId") String processId) throws Exception {
		return mapper.getApplicationByProcessId(processId);
	}

	@Override
	public void saveApplication(@Param("application") JobApplication application) throws Exception {
		mapper.saveApplication(application);
	}

	@Override
	public void changeApplicationStatus(@Param("id") Integer id, @Param("status") JobApplicationEnum status)
			throws Exception {
		mapper.changeApplicationStatus(id, status);
	}

	@Override
	public void changeMeetingDate(@Param("id") Integer id, @Param("date") Date date) throws Exception {
		mapper.changeMeetingDate(id, date);
	}

	@Override
	public void changeHireDate(@Param("id") Integer id, @Param("date") Date date) throws Exception {
		mapper.changeHireDate(id, date);
	}

}