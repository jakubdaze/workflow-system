package com.workflow.system.view;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.dao.BillDao;
import com.workflow.system.dao.ContractorDao;
import com.workflow.system.model.Address;
import com.workflow.system.model.Bill;
import com.workflow.system.model.Contractor;
import com.workflow.system.model.Group;
import com.workflow.system.security.AES;
import com.workflow.system.security.AESService;

@Component
@Scope("session")
@Qualifier("contractorView")
public class ContractorView extends AbstractView implements Serializable {

	private static final long serialVersionUID = 7916579160796392595L;

	@Autowired
	private ContractorDao contractorDao;
	@Autowired
	private BillDao billDao;
	private Bill bill;

	private Contractor contractor;
	private Contractor editContractor;

	private List<Contractor> allContractors;

	public ContractorView() {
	}

	private void clear() {
		contractor = null;
		editContractor = null;
	}

	private boolean contractorExists(Contractor contractor) throws Exception {
		if (contractorDao.countContractor(AESService.encrypt(contractor)) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public String cancel() {
		return "contractors";
	}

	public String addContractor() {
		contractor = new Contractor();
		contractor.setAddress(new Address());
		bill = new Bill();
		return "add_contractor";
	}

	public String editContractor(Contractor contractor) {
		editContractor = contractor;
		return "edit_contractor";
	}

	public void detailContractor(Contractor contractor) {
		this.contractor = contractor;
	}

	public String saveContractor() {
		try {
			if (contractorExists(contractor)) {
				addGrowl("contractor.exists");
				return "";
			}
			this.contractor = AESService.encrypt(contractor);
			contractorDao.saveContractor(contractor);
			bill.setBillName("[" + AES.decrypt(contractor.getNip()) + "] " + AES.decrypt(contractor.getName()));
			Integer contractorId = contractorDao.getContractorByNip(contractor.getNip()).getContractorId();
			bill.setContractorId(contractorId);
			billDao.saveBill(bill);
			addGrowl("contractor.add.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("contractor.add.fail");
		}
		clear();
		return "contractors";
	}

	public String updateContractor() {
		try {
			if (contractorExists(editContractor)) {
				addGrowl("contractor.exists");
				return "";
			}
			contractorDao.updateContractor(AESService.encrypt(editContractor));
			billDao.updateFolderName(editContractor.getContractorId(),
					"[" + editContractor.getNip() + "] " + editContractor.getName());
			addGrowl("contractor.update.success");
		} catch (Exception e) {
			addGrowl("contractor.update.fail");
		}
		clear();
		return "contractors";
	}

	public void deleteContractor(Contractor contractor) {
		try {
			contractorDao.deleteContractor(contractor);
			billDao.deleteBillByContractorId(contractor.getContractorId());
			addGrowl("contractor.delete.success");
		} catch (Exception e) {
			addGrowl("contractor.delete.fail");
		}
		clear();
	}

	public List<Contractor> getAllContractors() {
		try {
			allContractors = AESService.decryptContractors(contractorDao.getAllContractors());
		} catch (Exception e) {
			addGrowl("contractor.getall.fail");
		}
		return allContractors;
	}

	public ContractorDao getContractorDao() {
		return contractorDao;
	}

	public void setContractorDao(ContractorDao contractorDao) {
		this.contractorDao = contractorDao;
	}

	public Contractor getContractor() {
		return contractor;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}

	public Contractor getEditContractor() {
		return editContractor;
	}

	public void setEditContractor(Contractor editContractor) {
		this.editContractor = editContractor;
	}

	public BillDao getBillDao() {
		return billDao;
	}

	public void setBillDao(BillDao billDao) {
		this.billDao = billDao;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}
}