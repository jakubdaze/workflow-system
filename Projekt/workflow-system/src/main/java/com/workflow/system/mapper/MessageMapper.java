package com.workflow.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.workflow.system.model.Message;
import com.workflow.system.model.User;

public interface MessageMapper {

	public Message getMessageById(@Param("messageId") Integer messageId) throws Exception;

	public List<Message> getMessagesByReciepent(@Param("user") User user) throws Exception;
	
	public List<Message> getMessagesBySender(@Param("user") User user) throws Exception;
	
	public Integer countUnreadMessagesByUser(@Param("user") User user) throws Exception;
	
	public void saveMessage(@Param("message") Message message) throws Exception;
	
	public void updateMessage(@Param("message") Message message) throws Exception;
	
	public void deleteMessage(@Param("message") Message message) throws Exception;
	
	
	
}
