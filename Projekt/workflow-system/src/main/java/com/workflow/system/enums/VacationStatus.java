package com.workflow.system.enums;

public enum VacationStatus {
	PENDING("oczekuje"), ACCEPTED("zaakceptowany"), REJECTED("odrzucony");

	private String name;

	VacationStatus(String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
