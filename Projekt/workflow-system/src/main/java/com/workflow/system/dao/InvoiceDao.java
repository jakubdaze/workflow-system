package com.workflow.system.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.model.Bill;
import com.workflow.system.model.Invoice;
import com.workflow.system.enums.InvoiceStatus;
import com.workflow.system.mapper.InvoiceMapper;

@Service
public class InvoiceDao implements Serializable, InvoiceMapper {
	
	private static final long serialVersionUID = -8223483725065872151L;
	
	@Autowired
	private InvoiceMapper mapper;

	@Override
	public Invoice getInvoiceById(Integer id) throws Exception {
		return mapper.getInvoiceById(id);
	}

	@Override
	public Invoice getInvoiceByProcessId(String processId) throws Exception {
		return mapper.getInvoiceByProcessId(processId);
	}

	@Override
	public List<Invoice> getInvoicesByBill(Bill bill) throws Exception {
		return mapper.getInvoicesByBill(bill);
	}

	@Override
	public void changeInvoiceStatus(Integer invoiceId, InvoiceStatus status, String comment) throws Exception {
		mapper.changeInvoiceStatus(invoiceId, status, comment);
	}

	@Override
	public void changeInvoiceRegister(Integer invoiceId, Invoice invoice, InvoiceStatus status) throws Exception {
		mapper.changeInvoiceRegister(invoiceId, invoice, status);
	}

	@Override
	public void changeInvoiceDescribe(Integer invoiceId, Invoice invoice, InvoiceStatus status) throws Exception {
		mapper.changeInvoiceDescribe(invoiceId, invoice, status);
	}

	@Override
	public void changeInvoiceAccounting(Integer invoiceId, Invoice invoice, InvoiceStatus status) throws Exception {
		mapper.changeInvoiceAccounting(invoiceId, invoice, status);
	}

	@Override
	public void saveInvoice(Invoice invoice) throws Exception {
		mapper.saveInvoice(invoice);
	}

	@Override
	public void changeInvoiceComment(Integer invoiceId, String comment) throws Exception {
		mapper.changeInvoiceComment(invoiceId, comment);
	}

	@Override
	public void changeInvoiceDelete(Integer invoiceId, InvoiceStatus status) throws Exception {
		mapper.changeInvoiceDelete(invoiceId, status);
	}

	@Override
	public List<Invoice> getInvoicesByStatus(InvoiceStatus status) throws Exception {
		return mapper.getInvoicesByStatus(status);
	}

	@Override
	public List<Invoice> getInvoicesProccessed() throws Exception {
		return mapper.getInvoicesProccessed();
	}

	@Override
	public Invoice getInvoiceByBillId(Integer id) throws Exception {
		return mapper.getInvoiceByBillId(id);
	}

}