package com.workflow.system.activiti.listeners;

import java.util.Date;
import java.util.Timer;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.dao.UserDao;
import com.workflow.system.model.User;
import com.workflow.system.security.AES;
import com.workflow.system.timer.TerminationTimer;

@Service
public class DeleteTemporaryAccountListener implements JavaDelegate {

	@Autowired
	private UserDao userDao;

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		User user = userDao.getUserByEmail(AES.encrypt((String) execution.getVariable("user")));
		Timer timer = new Timer();
		timer.schedule(new TerminationTimer(user, userDao), new DateTime(new Date()).plusMinutes(2).toDate());
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
}
