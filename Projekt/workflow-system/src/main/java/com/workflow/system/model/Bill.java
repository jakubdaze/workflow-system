package com.workflow.system.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "bill")
public class Bill implements Serializable {

	private static final long serialVersionUID = 1321500008699221107L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "bill_id")
	private Integer billId;
	@Column(name = "parent_id")
	private Integer parentId;
	@Column(name = "bill_name")
	private String billName;
	@Column(name = "bill_content", columnDefinition = "LONGTEXT")
	private String billContent;
	@Column(name = "created_on")
	private Date createdOn;
	@Column(name = "size")
	private String size;
	@Column(name = "contractor_id")
	private Integer contractorId;

	public Bill() {

	}

	public Bill(Integer billId, Integer parentId, String billName, String billContent, Date createdOn, String size,
			Integer contractorId) {
		super();
		this.billId = billId;
		this.parentId = parentId;
		this.billName = billName;
		this.billContent = billContent;
		this.createdOn = createdOn;
		this.size = size;
		this.contractorId = contractorId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((billId == null) ? 0 : billId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bill other = (Bill) obj;
		if (billId == null) {
			if (other.billId != null)
				return false;
		} else if (!billId.equals(other.billId))
			return false;
		return true;
	}

	public Integer getBillId() {
		return billId;
	}

	public void setBillId(Integer billId) {
		this.billId = billId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getBillName() {
		return billName;
	}

	public void setBillName(String billName) {
		this.billName = billName;
	}

	public String getBillContent() {
		return billContent;
	}

	public void setBillContent(String billContent) {
		this.billContent = billContent;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Integer getContractorId() {
		return contractorId;
	}

	public void setContractorId(Integer contractorId) {
		this.contractorId = contractorId;
	}

}
