package com.workflow.system.permissions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workflow.system.dao.GroupDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.enums.PermissionEnum;
import com.workflow.system.model.Group;
import com.workflow.system.model.Permission;
import com.workflow.system.model.User;
import com.workflow.system.view.LoginView;

@Service
public class PermUtil {

	@Autowired
	private GroupDao groupDao;
	@Autowired
	private UserDao userDao;


	public List<Permission> getUserPermissions(User user) {
		try {
			return groupDao.getGroupPermissions(user.getGroup());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<User> getUsersByPermissions(List<Permission> permissions) {
		List<Group> groups;
		try {
			groups = groupDao.getGroupByPermissions(permissions);
			List<User> users = new ArrayList<User>();
			for (Group group : groups) {
				users.addAll(userDao.getUsersByGroup(group));
			}
			return users;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Permission> getVacationReviewersPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.VACATION_REVIEW));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getCVReviewersPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.CV_VIEW));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getCallApplicantPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.CALL_WITH_APPLICANT));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getMeetingApplicantPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.MEETING_WITH_APPLICANT));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getHireApplicantPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.EMPLOYMENT_DECISION));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}

	public List<Permission> getRequisitionAcceptPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.REQUISITION_ACCEPT));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getRequisitionRealizationPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.REQUISITION_REALIZATION));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getInvoiceRegisterPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.BILL_ADD));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getInvoiceDescribePermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.INVOICE_DESCRIBE));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getInvoiceAdmPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.INVOICE_ADM));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getInvoiceManagerPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.INVOICE_MANAGER));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getInvoiceMerchantPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.INVOICE_MERCHANT));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getInvoiceInternalControlPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.INVOICE_INTERNAL_CONTROL));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	public List<Permission> getInvoiceAccountingPermissions(){
		List<Permission> permissions = new ArrayList<Permission>();
		try {
			permissions.add(groupDao.getPermissionByName(PermissionEnum.BILL_ACCOUNTING));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return permissions;
	}
}
