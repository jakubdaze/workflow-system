package com.workflow.system.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.dao.BillDao;
import com.workflow.system.model.Bill;
import com.workflow.system.model.Permission;

@Component("billConverter")
@Scope("request")
public class BillConverter implements Converter {

	@Autowired
	private BillDao billDao;

	public BillConverter() {

	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isNotEmpty(value)) {
			final Integer id = Integer.parseInt(value);
			Bill bill;
			try {
				bill = billDao.getBillById(id);
				return bill;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			if (!(value instanceof Bill)) {
				throw new IllegalArgumentException(
						"value should be: " + Permission.class.getName() + ", but is: " + value.getClass().getName());
			}
			Bill bill = (Bill) value;

			if (bill.getBillId() != null) {
				final Integer id = bill.getBillId();
				return StringUtils.defaultString(id.toString());
			}
		}
		return "";
	}
}