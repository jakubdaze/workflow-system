package com.workflow.system.view;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.security.KeyStoreException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.activiti.services.InvoiceService;
import com.workflow.system.dao.BillDao;
import com.workflow.system.dao.ContractorDao;
import com.workflow.system.dao.InvoiceDao;
import com.workflow.system.enums.InvoiceStatus;
import com.workflow.system.model.Bill;
import com.workflow.system.model.Contractor;
import com.workflow.system.model.Invoice;
import com.workflow.system.model.User;
import com.workflow.system.security.AESService;
import com.workflow.system.security.CertificateService;

@Component
@Scope("session")
@Qualifier("billView")
public class BillView extends AbstractView implements Serializable {

	private static final long serialVersionUID = 3041318095491519440L;

	@Autowired
	private BillDao billDao;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private InvoiceDao invoiceDao;
	@Autowired
	private ContractorDao contractorDao;
	private User loggedUser;
	private Bill bill;
	private Bill selectedContractor;
	private UploadedFile file;
	private TreeNode root;
	private TreeNode selectedNode;
	private StreamedContent downloadFile;
	private String password;
	private Invoice invoice;
	private Bill selectedBill;
	private Contractor contractor;

	public BillView() {
	}

	public String addBill(User user) {
		bill = new Bill();
		loggedUser = user;
		selectedContractor = new Bill();
		return "add_bill";
	}

	public String allBills() {
		root = createDocuments();
		bill = new Bill();
		return "all_bills";
	}

	private void clear() {
		invoice = null;
	}
	
	public void detailInvoice() {
		selectedBill = (Bill) selectedNode.getData();
		try {
			this.invoice = invoiceDao.getInvoiceByBillId(selectedBill.getBillId());
			this.contractor = AESService.decrypt(contractorDao.getContractorById(selectedBill.getContractorId()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void uploadBill(FileUploadEvent event) {
		Date date = new Date();
		if (selectedContractor.getBillId() != null) {
			try {
				setFile(event.getFile());
				bill.setBillName(file.getFileName().substring(0, file.getFileName().length() - 4) + "-" + date.getTime()
						+ ".pdf");
				bill.setCreatedOn(new Date());
				bill.setBillContent(Base64.encodeBase64String(signBill()));
				bill.setParentId(selectedContractor.getBillId());
				bill.setSize(convertFileSize(file.getSize()));
				bill.setContractorId(selectedContractor.getContractorId());
				billDao.saveBill(bill);
				Bill uploadedBill = billDao.getBillByFileName(bill.getBillName());
				startInvoiceProcess(loggedUser, uploadedBill);
				addGrowl("bill.add.success");
			} catch (Exception e) {
				e.printStackTrace();
				addGrowl("bill.add.fail");
			}
		} else {
			addGrowl("bill.add.failcontractor");
		}
	}

	public void startInvoiceProcess(User user, Bill bill) {
		invoice = new Invoice();
		try {
			invoice.setUser(user);
			invoice.setBill(bill);
			String processId = invoiceService.startInvoiceProcess(invoice);
			if (processId != null && !processId.isEmpty()) {
				invoice.setStatus(InvoiceStatus.REGISTERING);
				invoice.setProcessId(processId);
				invoiceDao.saveInvoice(invoice);
				addGrowl("invoice.add.success");
			}
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.add.fail");
		}
		clear();
	}

	public byte[] signBill() {
		InputStream certStream = null;
		try {
			certStream = new ByteArrayInputStream(Base64.decodeBase64(loggedUser.getCert()));
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("bill.cert.error");
		}
		StreamedContent cert = new DefaultStreamedContent(certStream, "application/x-pkcs12");
		InputStream billStream = new ByteArrayInputStream(file.getContents());
		OutputStream signBill = null;
		try {
			signBill = CertificateService.signBill(billStream, cert, password);
		} catch (KeyStoreException e) {
			e.printStackTrace();
			addGrowl("bill.cert.passerror");
		}
		ByteArrayOutputStream bos = (ByteArrayOutputStream) signBill;
		return bos.toByteArray();
	}

	public void deleteBill() {
		try {
			billDao.deleteBill((Bill) selectedNode.getData());
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("bill.delete.fail");
		}
		selectedNode.getChildren().clear();
		selectedNode.getParent().getChildren().remove(selectedNode);
		selectedNode.setParent(null);
		selectedNode = null;
	}

	public void downloadBill() {
		if (selectedNode.getParent().getParent() != null) {
			Bill billFile = (Bill) selectedNode.getData();
			InputStream stream = new ByteArrayInputStream(Base64.decodeBase64(billFile.getBillContent()));
			downloadFile = new DefaultStreamedContent(stream, "application/pdf", billFile.getBillName());
		}
	}

	public static String convertFileSize(long size) {
		if (size <= 0)
			return "0";
		final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}

	public List<Bill> getAllContractors() {
		try {
			return billDao.getDirectories();
		} catch (Exception e) {
			addGrowl("bill.getallcontractor.fail");
		}
		return null;
	}

	public List<Bill> getBillsByContractor(int parentId) {
		try {
			return billDao.getBillsByParentId(parentId);
		} catch (Exception e) {
			addGrowl("bill.getbillsbycontractor.fail");
		}
		return null;
	}

	public TreeNode createDocuments() {
		TreeNode root = new DefaultTreeNode(new Bill(), null);

		List<Bill> directories = getAllContractors();
		for (Bill elementDir : directories) {
			TreeNode directory = new DefaultTreeNode(elementDir, root);
			List<Bill> files = getBillsByContractor(elementDir.getBillId());
			for (Bill elementFile : files) {
				TreeNode file = new DefaultTreeNode("bill", elementFile, directory);
			}
		}

		return root;
	}

	public String getRegistratorEmail() throws Exception {
		return loggedUser.getEmail();
	}

	public BillDao getBillDao() {
		return billDao;
	}

	public void setBillDao(BillDao billDao) {
		this.billDao = billDao;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public Bill getSelectedContractor() {
		return selectedContractor;
	}

	public void setSelectedContractor(Bill selectedContractor) {
		this.selectedContractor = selectedContractor;
	}

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public StreamedContent getDownloadFile() {
		return downloadFile;
	}

	public void setDownloadFile(StreamedContent downloadFile) {
		this.downloadFile = downloadFile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Bill getSelectedBill() {
		return selectedBill;
	}

	public void setSelectedBill(Bill selectedBill) {
		this.selectedBill = selectedBill;
	}

	public Contractor getContractor() {
		return contractor;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
	
}
