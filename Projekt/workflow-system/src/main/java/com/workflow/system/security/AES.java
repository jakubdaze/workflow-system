package com.workflow.system.security;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AES {
	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
	private static final String IV = "V{FA41;_ad51/47*";
	private static final String ENCRYPTION_KEY = "51_asD-><JxK,.%#";
	private static final String ENCODING = "UTF-8";

	public static String decrypt(final String value) {
		try {
			final Cipher cipher = Cipher.getInstance(AES.TRANSFORMATION);
			final SecretKeySpec key = new SecretKeySpec(AES.ENCRYPTION_KEY.getBytes(AES.ENCODING), AES.ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(AES.IV.getBytes(AES.ENCODING)));
			return new String(cipher.doFinal(new Base64().decode(value.getBytes(AES.ENCODING))), AES.ENCODING);
		} catch (Exception e) {
			return "";
		}
	}

	public static String encrypt(final String value) {
		try {
			final Cipher cipher = Cipher.getInstance(AES.TRANSFORMATION);
			final SecretKeySpec key = new SecretKeySpec(AES.ENCRYPTION_KEY.getBytes(AES.ENCODING), AES.ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(AES.IV.getBytes(AES.ENCODING)));
			return new Base64().encodeAsString(cipher.doFinal(value.getBytes(AES.ENCODING)));
		} catch (Exception e) {
			return "";
		}
	}
}