package com.workflow.system.activiti.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.workflow.system.dao.ApplicationDao;
import com.workflow.system.dao.MessageDao;
import com.workflow.system.enums.JobApplicationEnum;
import com.workflow.system.model.JobApplication;
import com.workflow.system.model.Message;
import com.workflow.system.model.Permission;
import com.workflow.system.model.User;
import com.workflow.system.security.AES;
import com.workflow.system.security.AESService;

@Service
public class JobApplicationService extends AbstractService {

	@Autowired
	private ApplicationDao dao;
	@Autowired
	private MessageDao messageDao;

	private String prepareCVReviewedMessageContent(JobApplicationEnum status) {
		StringBuilder builder = new StringBuilder();
		builder.append("Informujemy, że status Twojego podania o pracę po pierwszym etapie rekrutacji to: "
				+ status.getName());
		builder.append(System.getProperty("line.separator"));
		if (status.equals(JobApplicationEnum.ACCEPTED)) {
			builder.append(
					"Kolejnym etapem rekrutacji jest rozmowa telefoniczna z pracownikiem. Proszę czekać na telefon");
		} else {
			builder.append("Konto tymczasowe zostanie usunięte w przeciągu doby.");
		}
		return builder.toString();
	}

	private String preparePhoneCallMadeMessageContent(JobApplicationEnum status, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		StringBuilder builder = new StringBuilder();
		builder.append(
				"Informujemy, że status Twojego podania o pracę po rozmowie telefonicznej to: " + status.getName());
		builder.append(System.getProperty("line.separator"));
		if (status.equals(JobApplicationEnum.ACCEPTED)) {
			builder.append("Kolejnym etapem rekrutacji jest spotkanie z pracownikiem.");
			builder.append(System.getProperty("line.separator"));
			builder.append("Przypominamy, że podczas rozmowy telefonicznej ustalona została data i godzina spotkania: "
					+ sdf.format(date));
		} else {
			builder.append("Konto tymczasowe zostanie usunięte w przeciągu doby.");
		}
		return builder.toString();
	}

	private String prepareMeetingFinishedMessageContent(JobApplicationEnum status, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		StringBuilder builder = new StringBuilder();
		builder.append(
				"Informujemy, że status Twojego podania o pracę po spotkaniu z pracownikiem to: " + status.getName());
		builder.append(System.getProperty("line.separator"));
		if (status.equals(JobApplicationEnum.ACCEPTED)) {
			builder.append("Gratulacje. Wszystkie etapy rekrutacji przebiegły pomyślnie.");
			builder.append(System.getProperty("line.separator"));
			builder.append("Przypominamy, że podczas spotkania ustalona została data i godzina podpisania umowy: "
					+ sdf.format(date));
			builder.append(System.getProperty("line.separator"));
			builder.append("Zapraszamy do siedziby firmy w w.w. terminie.");
		} else {
			builder.append("Konto tymczasowe zostanie usunięte w przeciągu doby.");
		}
		return builder.toString();
	}

	private Message prepareMessage(User user, JobApplicationEnum status, String content, String title) {
		Message message = new Message();
		message.setReciepent(user);
		message.setTitle(title);
		message.setSendDate(new Date());
		message.setRead(false);
		message.setContent(content);
		return message;
	}

	public String startEmploymentProcess(User user) throws Exception {
		Map<String, Object> variables = Maps.newHashMap();
		variables.put("user", AES.decrypt(user.getEmail()));
		return runtimeService.startProcessInstanceByKey("employmentProcess", variables).getId();
	}

	public void reviewCv(Task task, JobApplicationEnum status) throws Exception {
		JobApplication application = dao.getApplicationByProcessId(task.getProcessInstanceId());
		messageDao.saveMessage(prepareMessage(application.getUser(), status, prepareCVReviewedMessageContent(status),
				"Twoje CV zostało zweryfikowane"));
		taskService.setVariable(task.getId(), "status", status.name());
		taskService.complete(task.getId());
	}

	public void doAfterCall(Task task, JobApplicationEnum status, Date date) throws Exception {
		JobApplication application = dao.getApplicationByProcessId(task.getProcessInstanceId());
		if (date != null) {
			dao.changeMeetingDate(application.getJobApplicationId(), date);
		}
		messageDao.saveMessage(
				prepareMessage(application.getUser(), status, preparePhoneCallMadeMessageContent(status, date),
						"Informacja po przeprowadzonej rozmowie telefonicznej"));
		taskService.setVariable(task.getId(), "status", status.name());
		taskService.complete(task.getId());
	}

	public void doAfterMeeting(Task task, JobApplicationEnum status, Date date) throws Exception {
		JobApplication application = dao.getApplicationByProcessId(task.getProcessInstanceId());
		if (date != null) {
			dao.changeHireDate(application.getJobApplicationId(), date);
		}
		messageDao.saveMessage(prepareMessage(application.getUser(), status,
				prepareMeetingFinishedMessageContent(status, date), "Informacja po przeprowadzonym spotkaniu"));
		dao.changeApplicationStatus(application.getJobApplicationId(), status);
		runtimeService.setVariable(task.getProcessInstanceId(), "status", status.name());
		taskService.complete(task.getId());
	}

	public List<String> findCVReviewers() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getCVReviewersPermissions();
		try {
			List<User> users = AESService.decryptUsers(permUtil.getUsersByPermissions(permissions));
			for (User user : users) {
				managers.add(user.getEmail());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}

	public List<String> findCallApplicantPermitedEmployees() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getCallApplicantPermissions();
		try {
			List<User> users = AESService.decryptUsers(permUtil.getUsersByPermissions(permissions));
			for (User user : users) {
				managers.add(user.getEmail());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}

	public List<String> findMeetApplicantPermitedEmployees() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getMeetingApplicantPermissions();
		try {
			List<User> users = AESService.decryptUsers(permUtil.getUsersByPermissions(permissions));
			for (User user : users) {
				managers.add(user.getEmail());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}

	public List<String> findHireApplicantPermitedEmployees() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getHireApplicantPermissions();
		try {
			List<User> users = AESService.decryptUsers(permUtil.getUsersByPermissions(permissions));
			for (User user : users) {
				managers.add(user.getEmail());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}
}
