package com.workflow.system.beans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
@Qualifier("navigationBean")
public class NavigationBean {

	public String goToGroups() {
		return "groups";
	}

	public String goToUsers() {
		return "users";
	}

	public String goToContractors() {
		return "contractors";
	}

	public String goToMessages() {
		return "messages";
	}

	public String goToBills() {
		return "bills";
	}

	public String goToTasks() {
		return "tasks";
	}

	public String goToMyTasks() {
		return "my_tasks";
	}

	public String goToHistoricalTasks() {
		return "historical_tasks";
	}

	public String goToVacations() {
		return "vacations";
	}
	
	public String goToRequisitions() {
		return "requisitions";
	}
	
	public String goToAccounted() {
		return "accounted_invoices";
	}
	
	public String goToProccessed() {
		return "proccessed_invoices";
	}

	public String goToHomepage() {
		return "homepage";
	}
}
