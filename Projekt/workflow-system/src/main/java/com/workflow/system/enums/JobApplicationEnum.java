package com.workflow.system.enums;

public enum JobApplicationEnum {
	ACCEPTED("Zaakceptowane"), REJECTED("Odrzucone");

	private String name;

	JobApplicationEnum(String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
