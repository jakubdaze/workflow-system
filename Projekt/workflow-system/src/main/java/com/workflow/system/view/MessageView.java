package com.workflow.system.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.dao.MessageDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.model.Message;
import com.workflow.system.model.User;
import com.workflow.system.security.AESService;

@Component
@Scope("session")
@Qualifier("messageView")
public class MessageView extends AbstractView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 103229121996704760L;

	@Autowired
	private UserDao userDao;

	@Autowired
	private LoginView loginView;

	@Autowired
	private MessageDao messageDao;

	private User selectedReciepent;

	private Message message;

	private List<User> allUsers;

	private boolean disabledReciepent;

	public MessageView() {
	}

	private void clear() {
		selectedReciepent = null;
		setMessage(null);
	}

	public String cancel() {
		return "messages";
	}

	public String answerToMessage(Message message) {
		Message toAnswer = message;
		if (toAnswer.getSender() == null) {
			addGrowl("message.no.sender");
			return "messages";
		}
		this.message = new Message();
		this.message.setReciepent(toAnswer.getSender());
		this.message.setTitle("RE: " + toAnswer.getTitle());
		disabledReciepent = true;
		return "add_message";
	}

	public String addMessage() {
		message = new Message();
		selectedReciepent = new User();
		disabledReciepent = false;
		return "add_message";
	}

	public void detailMessage(Message message) {
		try {
			messageDao.updateMessage(message);
		} catch (Exception e) {
			addGrowl("message.update.fail");
		}
		this.message = message;
	}

	public void detailSentMessage(Message message) {
		this.message = message;
	}

	public Integer getAmountOfUnreadMessages() {
		try {
			return messageDao.countUnreadMessagesByUser(loginView.getLoggedUser());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String saveMessage() {
		try {
			message.setRead(false);
			message.setSendDate(new Date());
			message.setSender(loginView.getLoggedUser());
			if (!disabledReciepent) {
				message.setReciepent(selectedReciepent);
			}
			messageDao.saveMessage(message);
			addGrowl("message.add.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("message.add.fail");
		}
		return "messages";
	}

	public void deleteMessage(Message message) {
		try {
			messageDao.deleteMessage(message);
			addGrowl("message.delete.success");
		} catch (Exception e) {
			addGrowl("message.delete.fail");
		}
		clear();
	}

	public List<User> getAllUsers() {
		try {
			allUsers = AESService.decryptUsers(userDao.getEveryUser());
		} catch (Exception e) {
			addGrowl("user.getall.fail");
		}
		return allUsers;
	}

	public List<Message> getReceivedMessages() {
		try {
			List<Message> messages = messageDao.getMessagesByReciepent(loginView.getLoggedUser());
			List<Message> decrypted = new ArrayList<Message>();
			for (Message message : messages) {
				if(message.getSender() != null){
					message.setSender(AESService.decrypt(message.getSender()));
				}
				decrypted.add(message);
			}
			return decrypted;
		} catch (Exception e) {
			addGrowl("messages.received.fail");
		}
		return new ArrayList<Message>();
	}

	public List<Message> getSentMessages() {
		try {
			List<Message> messages = messageDao.getMessagesBySender(loginView.getLoggedUser());
			List<Message> decrypted = new ArrayList<Message>();
			for (Message message : messages) {
				message.setReciepent(AESService.decrypt(message.getReciepent()));
				decrypted.add(message);
			}
			return decrypted;
		} catch (Exception e) {
			addGrowl("messages.sent.fail");
		}
		return new ArrayList<Message>();
	}

	public void onRowSelect(SelectEvent event) {
		detailMessage(message);
	}

	public void onRowSelectSent(SelectEvent event) {
		detailSentMessage(message);
	}

	public void onRowUnselect(UnselectEvent event) {
		clear();
	}

	public void setAllUsers(List<User> allUsers) {
		this.allUsers = allUsers;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public MessageDao getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}

	public User getSelectedReciepent() {
		return selectedReciepent;
	}

	public void setSelectedReciepent(User selectedReciepent) {
		this.selectedReciepent = selectedReciepent;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public LoginView getLoginView() {
		return loginView;
	}

	public void setLoginView(LoginView loginView) {
		this.loginView = loginView;
	}

	public boolean isDisabledReciepent() {
		return disabledReciepent;
	}

	public void setDisabledReciepent(boolean disabledReciepent) {
		this.disabledReciepent = disabledReciepent;
	}
}