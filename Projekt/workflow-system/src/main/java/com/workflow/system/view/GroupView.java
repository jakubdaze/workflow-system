package com.workflow.system.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.workflow.system.dao.GroupDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.model.Group;
import com.workflow.system.model.Permission;
import com.workflow.system.model.User;

@Component
@Scope("session")
@Qualifier("groupView")
public class GroupView extends AbstractView implements Serializable {

	private static final long serialVersionUID = 7916579160796392595L;

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private UserDao userDao;

	private Group group;
	private Group editGroup;

	private List<Group> allGroups;
	private List<Permission> allPermissions;
	private List<Permission> selectedPermissions;

	public GroupView() {
	}

	private void clear() {
		selectedPermissions = null;
		group = null;
		editGroup = null;
	}
	
	private boolean groupExists(Group group) throws Exception{
		if(groupDao.countGroup(group) > 0){
			return true;
		}else{
			return false;
		}
	}

	public String addGroup() {
		group = new Group();
		group.setPermissions(new ArrayList<Permission>());
		return "add_group";
	}

	public String editGroup(Group group) {
		selectedPermissions = group.getPermissions();
		setEditGroup(group);
		return "edit_group";
	}

	public void detailGroup(Group group) {
		this.group = group;
	}

	public String cancel() {
		return "cancel";
	}

	public List<Group> getAllGroups() {
		try {
			allGroups = groupDao.getAllGroups();
		} catch (Exception e) {
			addGrowl("group.getall.fail");
		}
		return allGroups;
	}

	public String saveGroup() {
		try {
			if(groupExists(group)){
				addGrowl("group.exists");
				return "";
			}
			groupDao.saveGroup(group);
			if (selectedPermissions != null && !selectedPermissions.isEmpty()) {
				groupDao.savePermissions(groupDao.getGroupByName(group), selectedPermissions);
			}
			addGrowl("group.add.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("group.add.fail");
		}
		clear();
		return "groups";
	}

	public String updateGroup() {
		try {
			if(groupExists(editGroup)){
				addGrowl("group.exists");
				return "";
			}
			groupDao.updateGroup(editGroup);
			groupDao.deleteGroupPermissions(editGroup);
			if (selectedPermissions != null && !selectedPermissions.isEmpty()) {
				groupDao.savePermissions(editGroup, selectedPermissions);
			}
			addGrowl("group.update.success");
		} catch (Exception e) {
			addGrowl("group.update.fail");
		}
		clear();
		return "groups";
	}

	public String deleteGroup(Group group) {
		try {
			List<User> users = userDao.getUsersByGroup(group);
			for(User user : users){
				user.setGroup(null);
				userDao.updateUser(user);
			}
			groupDao.deleteGroupPermissions(group);
			groupDao.deleteGroup(group);
			addGrowl("group.delete.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("group.delete.fail");
		}
		clear();
		return "groups";
	}

	public void setAllGroups(List<Group> allGroups) {
		this.allGroups = allGroups;
	}

	public GroupDao getGroupDao() {
		return groupDao;
	}

	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Group getEditGroup() {
		return editGroup;
	}

	public void setEditGroup(Group editGroup) {
		this.editGroup = editGroup;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public List<Permission> getAllPermissions() {
		try {
			return groupDao.getAllPermissions();
		} catch (Exception e) {
			addGrowl("permission.getall.fail");
		}
		return allPermissions;
	}

	public void setAllPermissions(List<Permission> allPermissions) {
		this.allPermissions = allPermissions;
	}

	public List<Permission> getSelectedPermissions() {
		return selectedPermissions;
	}

	public void setSelectedPermissions(List<Permission> selectedPermissions) {
		this.selectedPermissions = selectedPermissions;
	}

}