package com.workflow.system.view;

import java.awt.image.ImagingOpException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.activiti.services.TerminationService;
import com.workflow.system.converter.ImageConverter;
import com.workflow.system.dao.GroupDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.enums.TerminationType;
import com.workflow.system.model.Address;
import com.workflow.system.model.Group;
import com.workflow.system.model.User;
import com.workflow.system.security.AESService;
import com.workflow.system.security.SHA256;

@Component
@Scope("session")
@Qualifier("userView")
public class UserView extends AbstractView implements Serializable {

	private static final long serialVersionUID = 7916579160796392595L;

	@Autowired
	private UserDao userDao;

	@Autowired
	private TerminationService terminationService;

	@Autowired
	private GroupDao groupDao;

	private String fileContent;
	private UploadedFile file;
	private User user;
	private User editUser;
	private Group selectedGroup;
	private StreamedContent avatar;
	private Integer editUserId;

	private List<User> allUsers;

	private UploadedFile certFile;
	private String certFileContent;
	private TerminationType terminationType;
	private String motivation;

	public String getMotivation() {
		return motivation;
	}

	public void setMotivation(String motivation) {
		this.motivation = motivation;
	}

	public UserView() {
	}

	private void clear() {
		user = null;
		editUser = null;
		selectedGroup = null;
	}

	private boolean userExists(User user) throws Exception {
		if (userDao.countUser(AESService.encrypt(user)) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public String cancel() {
		return "users";
	}

	public String addUser() {
		user = new User();
		selectedGroup = new Group();
		user.setAddress(new Address());
		return "add_user";
	}

	public String editUser(User user) {
		editUser = user;
		editUserId = user.getUserId();
		selectedGroup = user.getGroup();
		return "edit_user";
	}

	public void upload(FileUploadEvent event) throws IllegalArgumentException, ImagingOpException, IOException {
		this.file = event.getFile();
		this.fileContent = ImageConverter.scaleImage(Base64.encodeBase64String(file.getContents()), 80, 80);
	}

	public void detailUser(User user) {
		avatar = null;
		try {
			this.user = userDao.getUserById(user.getUserId());
			this.user = AESService.decrypt(this.user);
			if (this.user.getAvatar() != null) {
				InputStream stream = new ByteArrayInputStream(Base64.decodeBase64(this.user.getAvatar()));
				avatar = new DefaultStreamedContent(stream);
			}
		} catch (Exception e) {
			addGrowl("user.detail.fail");
		}
	}

	public String saveUser() {
		try {
			user.setPassword(SHA256.hash(user.getPassword()));
			user.setGroup(selectedGroup);
			if (certFileContent != null) {
				user.setCert(certFileContent);
			}
			if (fileContent != null) {
				user.setAvatar(fileContent);
			}

			if (userExists(user)) {
				addGrowl("user.exists");
				return "";
			}
			userDao.saveUser(AESService.encrypt(user));
			addGrowl("user.add.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("user.add.fail");
		}
		clear();
		return "return_users";
	}

	public String updateUser() {
		try {
			if (fileContent != null) {
				editUser.setAvatar(ImageConverter.scaleImage(Base64.encodeBase64String(file.getContents()), 80, 80));
			}
			if (userExists(editUser)) {
				if (editUser.getUserId() != editUserId) {
					addGrowl("user.exists");
					return "";
				}
			}
			editUser.setGroup(selectedGroup);
			if (certFileContent != null) {
				editUser.setCert(Base64.encodeBase64String(certFile.getContents()));
			}
			userDao.updateUser(AESService.encrypt(editUser));
			addGrowl("user.update.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("user.update.fail");
		}
		clear();
		return "return_users";
	}

	public String deleteUser(User user) {
		this.user = user;
		return "terminate_user";
	}

	public String terminateUser() throws Exception {
		terminationService.startTerminationProcess(user, terminationType, motivation);
		addGrowl("msg.termination.process.start", user.getFirstName() + " " + user.getLastName());
		return "return_users";
	}

	public List<User> getAllUsers() {
		try {
			allUsers = AESService.decryptUsers(userDao.getAllUsers());
		} catch (Exception e) {
			addGrowl("user.getall.fail");
		}
		return allUsers;
	}

	public void setAllUsers(List<User> allUsers) {
		this.allUsers = allUsers;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public List<Group> getAllGroups() {
		try {
			return groupDao.getAllGroups();
		} catch (Exception e) {
			addGrowl("group.getall.fail");
		}
		return null;
	}

	public List<TerminationType> getTerminationTypes() {
		return Arrays.asList(TerminationType.values());
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public Date getMaxDate() {
		return new Date();
	}

	public Date getMinDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		try {
			date = sdf.parse("01-01-1900");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public void certUpload(FileUploadEvent event) {
		this.certFile = event.getFile();
		this.certFileContent = Base64.encodeBase64String(certFile.getContents());
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getEditUser() {
		return editUser;
	}

	public void setEditUser(User editUser) {
		this.editUser = editUser;
	}

	public GroupDao getGroupDao() {
		return groupDao;
	}

	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}

	public Group getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(Group selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public StreamedContent getAvatar() {
		return avatar;
	}

	public void setAvatar(StreamedContent avatar) {
		this.avatar = avatar;
	}

	public String getContent() {
		return fileContent;
	}

	public void setContent(String content) {
		this.fileContent = content;
	}

	public Integer getEditUserId() {
		return editUserId;
	}

	public void setEditUserId(Integer editUserId) {
		this.editUserId = editUserId;
	}

	public TerminationService getTerminationService() {
		return terminationService;
	}

	public void setTerminationService(TerminationService terminationService) {
		this.terminationService = terminationService;
	}

	public TerminationType getType() {
		return terminationType;
	}

	public void setType(TerminationType type) {
		this.terminationType = type;
	}
}