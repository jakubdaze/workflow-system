package com.workflow.system.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.dao.UserDao;
import com.workflow.system.model.User;
import com.workflow.system.security.AESService;

@Component("userConverter")
@Scope("request")
public class UserConverter implements Converter {

	@Autowired
	private UserDao userDao;

	public UserConverter() {

	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isNotEmpty(value)) {
			final Integer id = Integer.parseInt(value);
			User user;
			try {
				user = userDao.getUserById(id);
				user = AESService.decrypt(user);
				return user;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			if (!(value instanceof User)) {
				throw new IllegalArgumentException(
						"value should be: " + User.class.getName() + ", but is: " + value.getClass().getName());
			}
			User user = (User) value;

			if (user.getUserId() != null) {
				final Integer id = user.getUserId();
				return StringUtils.defaultString(id.toString());
			}
		}
		return "";
	}
}