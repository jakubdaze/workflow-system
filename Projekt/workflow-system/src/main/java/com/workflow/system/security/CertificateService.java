package com.workflow.system.security;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;

import org.primefaces.model.StreamedContent;

import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfSignatureAppearance;
import com.lowagie.text.pdf.PdfStamper;

public class CertificateService {

	private static PrivateKey privateKey;
	private static Certificate[] certificateChain;

	public static OutputStream signBill(InputStream file, StreamedContent certificate, String password) throws KeyStoreException{
		readKey(certificate.getStream(), password);
		PdfReader reader = null;
		try {
			reader = new PdfReader(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		OutputStream outputStream = new ByteArrayOutputStream();
		PdfStamper stp = null;
		try {
			stp = PdfStamper.createSignature(reader, outputStream, '\0');
			PdfSignatureAppearance sap = stp.getSignatureAppearance();
			sap.setCrypto(privateKey, certificateChain, null, PdfSignatureAppearance.WINCER_SIGNED);
			stp.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputStream;
	}

	protected static void readKey(InputStream cert, String pkcs12Password) throws KeyStoreException {
		KeyStore ks = null;
		try {
			ks = KeyStore.getInstance("pkcs12");
			ks.load(cert, pkcs12Password.toCharArray());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String alias = "";
		try {
			alias = (String) ks.aliases().nextElement();
			privateKey = (PrivateKey) ks.getKey(alias, pkcs12Password.toCharArray());
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		}
		certificateChain = ks.getCertificateChain(alias);
	}

}
