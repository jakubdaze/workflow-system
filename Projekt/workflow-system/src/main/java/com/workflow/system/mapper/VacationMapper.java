package com.workflow.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.workflow.system.enums.VacationStatus;
import com.workflow.system.model.User;
import com.workflow.system.model.Vacation;

public interface VacationMapper {

	public Vacation getVacationById(@Param("id") Integer id) throws Exception;

	public Vacation getVacationByProcessId(@Param("processId") String processId) throws Exception;

	public List<Vacation> getVacationsByUser(@Param("user") User user) throws Exception;
	
	public List<Vacation> getApprovedVacationsByUser(@Param("user") User user) throws Exception;

	public void changeVacationRequestStatus(@Param("id") Integer vacationId, @Param("status") VacationStatus status)
			throws Exception;

	public Integer countUserVacations(@Param("user") User user) throws Exception;

	public void saveVacation(@Param("vacation") Vacation vacation) throws Exception;
	
	public Integer countPendingVacationsByUser(@Param("user") User user) throws Exception;
}
