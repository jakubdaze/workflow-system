package com.workflow.system.enums;

public enum TerminationType {
	IMMIDIATE("Dyscyplinarne"), NORMAL("30-dniowe");

	private String name;

	TerminationType(String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
