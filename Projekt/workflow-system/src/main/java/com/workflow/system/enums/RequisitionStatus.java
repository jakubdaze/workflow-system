package com.workflow.system.enums;

public enum RequisitionStatus {
	DESCRIBING, ACCEPTED, REJECTED, DESCRIBED, REALIZED, REALIZING
}
