package com.workflow.system.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.workflow.system.enums.RequisitionStatus;

@Entity
@Table(name = "requisition")
public class Requisition implements Serializable {

	private static final long serialVersionUID = -6280521334741080252L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "requisition_id")
	private Integer requisitionId;

	@Column(name = "requisition_date")
	private Date requisitionDate;

	@Column(name = "items")
	private String items;
	
	@Column(name = "amount")
	private String amount;
	
	@Column(name = "comment")
	private String comment;

	@Column(name = "process_id")
	private String processId;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private RequisitionStatus status;

	@ManyToOne
	private User user;

	public Requisition() {

	}

	public Requisition(Date requisitionDate, String items, String amount, String comment, User user) {
		super();
		this.requisitionDate = requisitionDate;
		this.items = items;
		this.amount = amount;
		this.comment = comment;
		this.user = user;
	}

	public Integer getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(Integer requisitionId) {
		this.requisitionId = requisitionId;
	}

	public Date getRequisitionDate() {
		return requisitionDate;
	}

	public void setRequisitionDate(Date requisitionDate) {
		this.requisitionDate = requisitionDate;
	}

	public String getItems() {
		return items;
	}

	public void setItems(String items) {
		this.items = items;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public RequisitionStatus getStatus() {
		return status;
	}

	public void setStatus(RequisitionStatus status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
