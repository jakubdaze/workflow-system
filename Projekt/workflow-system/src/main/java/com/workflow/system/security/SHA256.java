package com.workflow.system.security;

import org.apache.commons.codec.digest.DigestUtils;

import com.workflow.system.constants.Constants;

public class SHA256 {

	public static String hash(String string) {
		return DigestUtils.sha256Hex(string + Constants.SALT);
	}
}
