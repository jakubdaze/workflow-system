package com.workflow.system.activiti.services;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;

import com.workflow.system.dao.GroupDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.model.Permission;
import com.workflow.system.model.User;
import com.workflow.system.permissions.PermUtil;
import com.workflow.system.security.AES;

public abstract class AbstractService {
	
	@Autowired
	protected UserDao userDao;
	
	@Autowired
	protected GroupDao groupDao;
	
	@Autowired
	protected PermUtil permUtil;

	@Autowired
	protected ProcessEngine engine;

	@Autowired
	protected RuntimeService runtimeService;

	@Autowired
	protected TaskService taskService;
	
	@Autowired
	protected HistoryService historyService;

	public List<String> findAllEmployees() {
		List<String> employees = new ArrayList<String>();
		try {
			List<User> users = userDao.getAllUsers();
			for (User user : users) {
				employees.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employees;
	}

	public List<String> findAllVacationReviewers() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getVacationReviewersPermissions();
		try {
			List<User> users = permUtil.getUsersByPermissions(permissions);
			for (User user : users) {
				managers.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}

	public List<String> findAllRequisitionAccepters() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getRequisitionAcceptPermissions();
		try {
			List<User> users = permUtil.getUsersByPermissions(permissions);
			for (User user : users) {
				managers.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}
	
	public List<String> findAllRequisitionRealizators() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getRequisitionRealizationPermissions();
		try {
			List<User> users = permUtil.getUsersByPermissions(permissions);
			for (User user : users) {
				managers.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}
	
	public List<String> findAllInvoiceDescribers() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getInvoiceDescribePermissions();
		try {
			List<User> users = permUtil.getUsersByPermissions(permissions);
			for (User user : users) {
				managers.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}
	
	public List<String> findAllInvoiceManagerAccepters() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getInvoiceManagerPermissions();
		try {
			List<User> users = permUtil.getUsersByPermissions(permissions);
			for (User user : users) {
				managers.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}
	
	public List<String> findAllInvoiceAdmAccepters() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getInvoiceAdmPermissions();
		try {
			List<User> users = permUtil.getUsersByPermissions(permissions);
			for (User user : users) {
				managers.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}
	
	public List<String> findAllInvoiceMerchantAccepters() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getInvoiceMerchantPermissions();
		try {
			List<User> users = permUtil.getUsersByPermissions(permissions);
			for (User user : users) {
				managers.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}
	
	public List<String> findAllInvoiceInternalControl() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getInvoiceInternalControlPermissions();
		try {
			List<User> users = permUtil.getUsersByPermissions(permissions);
			for (User user : users) {
				managers.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}
	
	public List<String> findAllInvoiceAccounters() {
		List<String> managers = new ArrayList<String>();
		List<Permission> permissions = permUtil.getInvoiceAccountingPermissions();
		try {
			List<User> users = permUtil.getUsersByPermissions(permissions);
			for (User user : users) {
				managers.add(AES.decrypt(user.getEmail()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return managers;
	}
	
	public List<Task> getAllTasksByAssignee(User user) throws Exception {
		return taskService.createTaskQuery().taskAssignee(user.getEmail()).list();
	}

	public List<Task> getAllTasksByCandidate(User user) throws Exception {
		return taskService.createTaskQuery().taskCandidateUser(user.getEmail()).list();
	}
	
	public List<HistoricTaskInstance> getAllHistoricalTasks(User user) throws Exception{
		return historyService.createHistoricTaskInstanceQuery().finished().taskAssignee(user.getEmail()).list();
	}
	
	public List<Task> getAllTasks() {
		return taskService.createTaskQuery().list();
	}

	public void claimTask(Task task, User user) throws Exception {
		taskService.claim(task.getId(), user.getEmail());
		task.setAssignee(user.getEmail());
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public ProcessEngine getEngine() {
		return engine;
	}

	public void setEngine(ProcessEngine engine) {
		this.engine = engine;
	}
}
