package com.workflow.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.workflow.system.enums.RequisitionStatus;
import com.workflow.system.model.Requisition;
import com.workflow.system.model.User;

public interface RequisitionMapper {

	public Requisition getRequisitionById(@Param("id") Integer id) throws Exception;

	public Requisition getRequisitionByProcessId(@Param("processId") String processId) throws Exception;

	public List<Requisition> getRequisitionsByUser(@Param("user") User user) throws Exception;

	public List<Requisition> getApprovedRequisitionsByUser(@Param("user") User user) throws Exception;

	public List<Requisition> getRealizedRequisitionsByUser(@Param("user") User user) throws Exception;

	public void changeRequisitionRequestStatus(@Param("id") Integer requisitionId,
			@Param("status") RequisitionStatus status) throws Exception;

	public void changeRequisitionRequestDescribe(@Param("id") Integer requisitionId,
			@Param("requisition") Requisition requisition, @Param("status") RequisitionStatus status) throws Exception;

	public void changeRequisitionRequestComment(@Param("id") Integer requisitionId,
			@Param("status") RequisitionStatus status, @Param("comment") String comment) throws Exception;

	public void saveRequisition(@Param("requisition") Requisition requisition) throws Exception;
}
