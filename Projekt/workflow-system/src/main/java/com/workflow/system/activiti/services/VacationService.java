package com.workflow.system.activiti.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.workflow.system.dao.MessageDao;
import com.workflow.system.dao.UserDao;
import com.workflow.system.dao.VacationDao;
import com.workflow.system.enums.VacationStatus;
import com.workflow.system.model.Message;
import com.workflow.system.model.User;
import com.workflow.system.model.Vacation;
import com.workflow.system.security.AES;

@Service
public class VacationService extends AbstractService {

	@Autowired
	private VacationDao vacationDao;
	@Autowired
	private MessageDao messageDao;
	@Autowired
	private UserDao userDao;

	private String prepareMessageContent(Vacation vacation, VacationStatus status) {
		StringBuilder builder = new StringBuilder();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		builder.append("Wniosek: ");
		builder.append(System.getProperty("line.separator"));
		builder.append("Data początkowa: " + sdf.format(vacation.getStartDate()));
		builder.append(System.getProperty("line.separator"));
		builder.append("Data końcowa: " + sdf.format(vacation.getEndDate()));
		builder.append(System.getProperty("line.separator"));
		builder.append("Uzasadnienie: " + vacation.getMotivation());
		builder.append(System.getProperty("line.separator"));
		builder.append("Decyzja przełożonego: Wniosek " + status.getName());
		return builder.toString();
	}

	private Message createDecisionMessage(Vacation vacation, VacationStatus status) throws Exception {
		Message message = new Message();
		message.setRead(false);
		message.setReciepent(vacation.getUser());
		message.setContent(prepareMessageContent(vacation, status));
		message.setTitle("Decyzja dotycząca Twojego wniosku o urlop");
		message.setSendDate(new Date());
		return message;
	}

	public String addVacationRequest(Vacation vacation) throws Exception {
		Map<String, Object> variables = Maps.newHashMap();
		variables.put("user", vacation.getUser().getEmail());
		variables.put("start_date", vacation.getStartDate());
		variables.put("end_date", vacation.getEndDate());
		variables.put("motivation", vacation.getMotivation());
		variables.put("status", VacationStatus.PENDING);
		return runtimeService.startProcessInstanceByKey("vacationRequestProcess", variables).getId();
	}

	public void changeVacationRequestStatus(Task task, VacationStatus status) throws Exception {
		Vacation vacation = vacationDao.getVacationByProcessId(task.getProcessInstanceId());
		vacationDao.changeVacationRequestStatus(vacation.getVacationId(), status);
		messageDao.saveMessage(createDecisionMessage(vacation, status));
		taskService.setVariable(task.getId(), "status", status.toString());
		taskService.complete(task.getId());
	}
}
