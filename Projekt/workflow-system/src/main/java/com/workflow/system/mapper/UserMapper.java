package com.workflow.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.workflow.system.model.Group;
import com.workflow.system.model.User;

public interface UserMapper {

	public User getUserById(@Param("userId") Integer userId) throws Exception;
	
	public User getUserByEmail(@Param("email") String email) throws Exception;

	public List<User> getAllUsers() throws Exception;
	
	public List<User> getEveryUser() throws Exception;
	
	public List<User> getUsersByGroup(@Param("group") Group group) throws Exception;

	public void saveUser(@Param("user") User user) throws Exception;

	public void deleteUser(@Param("user") User user) throws Exception;

	public void updateUser(@Param("user") User user) throws Exception;

	public void changeUserPassword(@Param("user") User user, @Param("password") String password) throws Exception;

	public Integer countUser(@Param("user") User user) throws Exception;
}
