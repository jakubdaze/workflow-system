package com.workflow.system.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
@Qualifier("mainView")
public class MainView extends AbstractView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5982309419518903157L;

	private DashboardModel model;

	@PostConstruct
	public void init() {
		model = new DefaultDashboardModel();
		DashboardColumn column1 = new DefaultDashboardColumn();
		DashboardColumn column2 = new DefaultDashboardColumn();
		column1.addWidget("unasignedTasks");
		column2.addWidget("assignedTasks");
		model.addColumn(column1);
		model.addColumn(column2);
	}

	public DashboardModel getModel() {
		return model;
	}

	public void setModel(DashboardModel model) {
		this.model = model;
	}

}