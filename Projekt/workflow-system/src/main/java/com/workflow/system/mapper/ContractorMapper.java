package com.workflow.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.workflow.system.model.Contractor;

public interface ContractorMapper {

	public List<Contractor> getAllContractors() throws Exception;

	public Contractor getContractorByNip(@Param("nip") String nip) throws Exception;
	
	public Contractor getContractorById(@Param("id") Integer id) throws Exception;

	public Contractor getContractorByEmail(@Param("email") String email) throws Exception;

	public Contractor getContractorByPhoneNumber(@Param("phoneNumber") String phoneNumber) throws Exception;

	public void saveContractor(@Param("contractor") Contractor contractor) throws Exception;

	public void updateContractor(@Param("contractor") Contractor contractor) throws Exception;

	public void deleteContractor(@Param("contractor") Contractor contractor) throws Exception;
	
	public Integer countContractor(@Param("contractor") Contractor contractor) throws Exception;
}
