package com.workflow.system.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.workflow.system.security.AES;

@Entity
@Table(name = "contractor")
public class Contractor implements Serializable {

	private static final long serialVersionUID = 4955074761174725347L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "contractor_id")
	private Integer contractorId;
	@Column(name = "name")
	private String name;
	@Column(name = "nip")
	private String nip;
	@Column(name = "regon")
	private String regon;
	@Column(name = "krs")
	private String krs;
	@Column(name = "phone_number")
	private String phoneNumber;
	@Column(name = "email")
	private String email;
	@Embedded
	private Address address;

	public Contractor() {

	}

	public Contractor(String name, String nip, String regon, String krs, String phoneNumber, String email,
			Address address) {
		super();
		this.name = name;
		this.nip = nip;
		this.regon = regon;
		this.krs = krs;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
	}

	public Integer getContractorId() {
		return contractorId;
	}

	public void setContractorId(Integer contractorId) {
		this.contractorId = contractorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getRegon() {
		return regon;
	}

	public void setRegon(String regon) {
		this.regon = regon;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getKrs() {
		return krs;
	}

	public void setKrs(String krs) {
		this.krs = krs;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
