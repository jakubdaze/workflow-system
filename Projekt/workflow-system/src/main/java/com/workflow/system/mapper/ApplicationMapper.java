package com.workflow.system.mapper;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.workflow.system.enums.JobApplicationEnum;
import com.workflow.system.model.JobApplication;
import com.workflow.system.model.User;

public interface ApplicationMapper {

	public JobApplication getApplicationById(@Param("id") Integer id) throws Exception;

	public JobApplication getApplicationByUser(@Param("user") User user) throws Exception;

	public JobApplication getApplicationByProcessId(@Param("processId") String processId) throws Exception;

	public void saveApplication(@Param("application") JobApplication application) throws Exception;

	public void changeApplicationStatus(@Param("id") Integer id, @Param("status") JobApplicationEnum status)
			throws Exception;
	
	public void changeMeetingDate(@Param("id") Integer id, @Param("date") Date date)
			throws Exception;
	
	public void changeHireDate(@Param("id") Integer id, @Param("date") Date date)
			throws Exception;

}
