package com.workflow.system.view;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.workflow.system.bundle.ResourceBundle;

public abstract class AbstractView {

	protected void redirectLogin() throws Exception{
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		context.redirect(context.getRequestContextPath());
	}
	
	protected void addMessage(String msg, String key) {
		if (key != null) {
			FacesContext.getCurrentInstance().addMessage(key,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, ResourceBundle.getMessage(msg), null));
		} else {
			FacesContext.getCurrentInstance().addMessage(key, new FacesMessage(ResourceBundle.getMessage(msg)));
		}
	}

	protected void addGrowl(String msg) {
		FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(ResourceBundle.getMessage(msg)));
	}
	
	protected void addGrowl(String msg, String addnotation) {
		FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(ResourceBundle.getMessage(msg) + addnotation));
	}

	protected void closeDialog(String widgetVar) {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('" + widgetVar + "').hide();");
	}
	
	protected void showDialog(String widgetVar) {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('" + widgetVar + "').show();");
	}
}
