package com.workflow.system.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.workflow.system.activiti.services.InvoiceService;
import com.workflow.system.activiti.services.JobApplicationService;
import com.workflow.system.activiti.services.RequisitionService;
import com.workflow.system.activiti.services.VacationService;
import com.workflow.system.activiti.services.ViewService;
import com.workflow.system.constants.Constants;
import com.workflow.system.dao.ApplicationDao;
import com.workflow.system.dao.BillDao;
import com.workflow.system.dao.ContractorDao;
import com.workflow.system.dao.InvoiceDao;
import com.workflow.system.dao.RequisitionDao;
import com.workflow.system.dao.VacationDao;
import com.workflow.system.enums.InvoiceEnum;
import com.workflow.system.enums.InvoiceStatus;
import com.workflow.system.enums.JobApplicationEnum;
import com.workflow.system.enums.RequisitionStatus;
import com.workflow.system.enums.VacationStatus;
import com.workflow.system.model.Contractor;
import com.workflow.system.model.Invoice;
import com.workflow.system.model.JobApplication;
import com.workflow.system.model.User;
import com.workflow.system.model.Requisition;
import com.workflow.system.model.Vacation;
import com.workflow.system.security.AESService;

@Component
@Scope("session")
@Qualifier("taskView")
public class TaskView extends AbstractView implements Serializable {

	private static final long serialVersionUID = 6269380587057090165L;

	@Autowired
	private ViewService viewService;
	@Autowired
	private VacationService vacationService;
	@Autowired
	private RequisitionService requisitionService;
	@Autowired
	private VacationDao vacationDao;
	@Autowired
	private RequisitionDao requisitionDao;
	@Autowired
	private LoginView loginView;
	@Autowired
	private ApplicationDao applicationDao;
	@Autowired
	private InvoiceDao invoiceDao;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private ContractorDao contractorDao;
	@Autowired
	private BillDao billDao;
	@Autowired
	private JobApplicationService applicationService;
	private StreamedContent file;
	private JobApplicationEnum selectedStatus;
	private Date selectedDate;

	private Task currentTask;
	private Vacation vacation;
	private Requisition requisition;
	private JobApplication jobApplication;
	private Invoice invoice;
	private Contractor contractor;

	private void clear() {
		file = null;
		selectedDate = null;
		selectedStatus = null;
		currentTask = null;
		vacation = null;
		jobApplication = null;
		requisition = null;
		invoice = null;
	}

	private void prepareView(Task task) {
		if (task.getFormKey().equals(Constants.VACATION_REVIEW_FORM_KEY)) {
			try {
				vacation = vacationDao.getVacationByProcessId(task.getProcessInstanceId());
				vacation.setUser(AESService.decrypt(vacation.getUser()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (task.getFormKey().equals(Constants.CV_REVIEW_FORM_KEY)
				|| task.getFormKey().equals(Constants.APPLICANT_PHONE_CALL_FORM_KEY)
				|| task.getFormKey().equals(Constants.APPLICANT_MEETING_FORM_KEY)) {
			try {
				jobApplication = applicationDao.getApplicationByProcessId(task.getProcessInstanceId());
				jobApplication.setUser(AESService.decrypt(jobApplication.getUser()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (task.getFormKey().equals(Constants.REQUISITION_DESCRIBE_FORM_KEY)
				|| task.getFormKey().equals(Constants.REQUISITION_ACCEPTANCE_FORM_KEY)
				|| task.getFormKey().equals(Constants.REQUISITION_REALIZATION_FORM_KEY)) {
			try {
				requisition = requisitionDao.getRequisitionByProcessId(task.getProcessInstanceId());
				requisition.setUser(AESService.decrypt(requisition.getUser()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (task.getFormKey().equals(Constants.INVOICE_REGISTER_FORM_KEY)
				|| task.getFormKey().equals(Constants.INVOICE_DESCRIBE_FORM_KEY)
				|| task.getFormKey().equals(Constants.INVOICE_KDACCEPT_FORM_KEY)
				|| task.getFormKey().equals(Constants.INVOICE_KZACCEPT_FORM_KEY)
				|| task.getFormKey().equals(Constants.INVOICE_KKW_FORM_KEY)
				|| task.getFormKey().equals(Constants.INVOICE_KADESCRIBE_FORM_KEY)
				|| task.getFormKey().equals(Constants.INVOICE_ZKACCEPT_FORM_KEY)
				|| task.getFormKey().equals(Constants.INVOICE_ZDACCEPT_FORM_KEY)
				|| task.getFormKey().equals(Constants.INVOICE_ACCOUNTING_FORM_KEY)) {
			try {
				invoice = invoiceDao.getInvoiceByProcessId(task.getProcessInstanceId());
				invoice.setUser(AESService.decrypt(invoice.getUser()));
				contractor = AESService.decrypt(contractorDao.getContractorById(invoice.getBill().getContractorId()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void prepareStreamedContent(String base64, User user) throws Exception {
		InputStream stream = new ByteArrayInputStream(Base64.decodeBase64(base64));
		this.file = new DefaultStreamedContent(stream, "application/pdf", "CV" + user.getEmail() + ".pdf");
	}
	
	private void prepareBillStreamedContent(String base64, String name) throws IOException {
		InputStream stream = new ByteArrayInputStream(Base64.decodeBase64(base64));
		this.file = new DefaultStreamedContent(stream, "application/pdf", name);
	}

	private void navigateTo(String formKey) {
		FacesContext context = FacesContext.getCurrentInstance();
		NavigationHandler navigationHandler = context.getApplication().getNavigationHandler();
		navigationHandler.handleNavigation(context, null, formKey);
	}

	public void prepareFile() throws Exception {
		try {
			prepareStreamedContent(jobApplication.getCv(), jobApplication.getUser());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void prepareBillFile() {
		try {
			prepareBillStreamedContent(invoice.getBill().getBillContent(), invoice.getBill().getBillName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ViewService getViewService() {
		return viewService;
	}

	public void setViewService(ViewService viewService) {
		this.viewService = viewService;
	}

	public List<Task> getAllTasks() throws Exception {
		return viewService.getAllTasksByCandidate(loginView.getLoggedUser());
	}

	public List<Task> getTasksByAssignee() throws Exception {
		return viewService.getAllTasksByAssignee(loginView.getLoggedUser());
	}

	public void claimTask(Task task) {
		try {
			viewService.claimTask(task, loginView.getLoggedUser());
			currentTask = task;
			prepareView(task);
			navigateTo(task.getFormKey());
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("msg.vacation.task.fail");
		}
	}

	public void doTask(Task task) {
		try {
			currentTask = task;
			prepareView(task);
			navigateTo(task.getFormKey());
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("msg.vacation.task.fail");
		}
	}

	public List<HistoricTaskInstance> getHistoricalTasks() throws Exception {
		return viewService.getAllHistoricalTasks(loginView.getLoggedUser());
	}

	public String reviewCv() {
		try {
			applicationService.reviewCv(currentTask, selectedStatus);
			addGrowl("msg.cv.review.success");
		} catch (Exception e) {
			addGrowl("msg.cv.error");
			e.printStackTrace();
		}
		clear();
		return "tasks";
	}

	public String doAfterPhoneCall() {
		try {
			applicationService.doAfterCall(currentTask, selectedStatus, selectedDate);
			addGrowl("msg.cv.phone.success");
		} catch (Exception e) {
			addGrowl("msg.cv.error");
			e.printStackTrace();
		}
		clear();
		return "tasks";
	}

	public String doAfterMeeting() {
		try {
			JobApplication application = applicationDao.getApplicationByProcessId(currentTask.getProcessInstanceId());
			if (!application.getMeetingDate().before(new Date())) {
				addGrowl("msg.cv.meetdate.before");
				return "tasks";
			}
			applicationService.doAfterMeeting(currentTask, selectedStatus, selectedDate);
			addGrowl("msg.cv.meet.success");
		} catch (Exception e) {
			addGrowl("msg.cv.error");
			e.printStackTrace();
		}
		clear();
		return "tasks";
	}

	public String approveVacationRequest() {
		try {
			vacationService.changeVacationRequestStatus(currentTask, VacationStatus.ACCEPTED);
			addGrowl("msg.vacation.approved");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("vacation.approval.fail");
		}
		clear();
		return "tasks";
	}

	public String rejectVacationRequest() {
		try {
			vacationService.changeVacationRequestStatus(currentTask, VacationStatus.REJECTED);
			addGrowl("msg.vacation.rejected");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("vacation.rejection.fail");
		}
		clear();
		return "tasks";
	}

	public String describeRequisitionRequest() {
		try {
			requisitionService.describeRequisitionRequest(currentTask, requisition, RequisitionStatus.DESCRIBED);
			addGrowl("requisition.describe.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("requisition.describe.fail");
		}
		clear();
		return "tasks";
	}

	public String acceptRequisitionRequest() {
		try {
			requisitionService.changeRequisitionRequestComment(currentTask, RequisitionStatus.ACCEPTED,
					requisition.getComment());
			addGrowl("requisition.approval.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("requisition.approval.fail");
		}
		clear();
		return "tasks";
	}

	public String rejectRequisitionRequest() {
		try {
			requisitionService.changeRequisitionRequestComment(currentTask, RequisitionStatus.REJECTED,
					requisition.getComment());
			addGrowl("requisition.rejection.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("requisition.rejection.fail");
		}
		clear();
		return "tasks";
	}

	public String realizeRequisitionRequest() {
		try {
			requisitionService.changeRequisitionRequestStatus(currentTask, RequisitionStatus.REALIZED);
			addGrowl("requisition.realization.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("requisition.realization.fail");
		}
		clear();
		return "tasks";	
	}
	
	public String registerInvoice() {
		try {
			Date date = new Date();
			invoiceService.registerInvoice(currentTask, invoice, InvoiceStatus.REGISTERED);
			String newName = contractor.getNip() + "-" + invoice.getInvoiceNumber().trim() + "-" + date.getTime() + ".pdf";
			billDao.updateBillName(invoice.getBill().getBillId(), newName);
			addGrowl("invoice.register.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.register.fail");
		}
		clear();
		return "tasks";
	}
	
	public String deleteInvoice() {
		try {
			invoiceService.changeInvoiceDelete(currentTask, InvoiceStatus.DELETE);
			billDao.deleteBillById(invoice.getBill().getBillId());
			addGrowl("bill.delete.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("bill.delete.fail");
		}
		clear();
		return "tasks";
	}
	
	public String describeInvoice() {
		try {
			invoiceService.describeInvoice(currentTask, invoice, InvoiceStatus.DESCRIBED);
			addGrowl("invoice.describe.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.describe.fail");
		}
		clear();
		return "tasks";
	}
	
	public String toRegisterInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.TOREGISTER, invoice.getComment());
			addGrowl("invoice.describe.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.describe.fail");
		}
		clear();
		return "tasks";
	}
	
	public String kDAcceptInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.KDACCEPTED, invoice.getComment());
			addGrowl("invoice.accept.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.accept.fail");
		}
		clear();
		return "tasks";
	}
	
	public String kDRejectInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.KDREJECTED, invoice.getComment());
			addGrowl("invoice.reject.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.reject.fail");
		}
		clear();
		return "tasks";
	}
	
	public String kZAcceptInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.KZACCEPTED, invoice.getComment());
			addGrowl("invoice.accept.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.accept.fail");
		}
		clear();
		return "tasks";
	}
	
	public String kZRejectInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.KZREJECTED, invoice.getComment());
			addGrowl("invoice.reject.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.reject.fail");
		}
		clear();
		return "tasks";
	}
	
	public String kWAcceptInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.KKWACCEPTED, invoice.getComment());
			addGrowl("invoice.accept.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.accept.fail");
		}
		clear();
		return "tasks";
	}
	
	public String kWRejectInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.KKWREJECTED, invoice.getComment());
			addGrowl("invoice.reject.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.reject.fail");
		}
		clear();
		return "tasks";
	}
	
	public String kWADDescribeInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.TODESCRIBE, invoice.getComment());
			addGrowl("invoice.reject.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.reject.fail");
		}
		clear();
		return "tasks";
	}
	
	public String aDDescribeInvoice() {
		try {
			invoiceService.describeInvoice(currentTask, invoice, InvoiceStatus.KKWDESCRIBED);
			addGrowl("invoice.describe.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.describe.fail");
		}
		clear();
		return "tasks";
	}
	
	public String accountInvoice() {
		try {
			invoiceService.accountInvoice(currentTask, invoice, InvoiceStatus.ACCOUNTED);
			addGrowl("invoice.accounting.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.accounting.fail");
		}
		clear();
		return "tasks";
	}
	
	public String zKAcceptInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.ZKACCEPTED, invoice.getComment());
			addGrowl("invoice.accept.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.accept.fail");
		}
		clear();
		return "tasks";
	}
	
	public String zKRejectInvoice() {
		try {
			invoiceService.changeInvoiceStatus(currentTask, InvoiceStatus.ZKREJECTED, invoice.getComment());
			addGrowl("invoice.reject.success");
		} catch (Exception e) {
			e.printStackTrace();
			addGrowl("invoice.reject.fail");
		}
		clear();
		return "tasks";
	}

	public Date getMinDate() {
		return new DateTime(new Date()).plusDays(1).toDate();
	}

	public List<JobApplicationEnum> getApplicationStatuses() {
		return Arrays.asList(JobApplicationEnum.values());
	}
	
	public List<InvoiceEnum> getInvoiceTypes() {
		return Arrays.asList(InvoiceEnum.values());
	}

	public LoginView getLoginView() {
		return loginView;
	}

	public void setLoginView(LoginView loginView) {
		this.loginView = loginView;
	}

	public Task getCurrentTask() {
		return currentTask;
	}

	public void setCurrentTask(Task currentTask) {
		this.currentTask = currentTask;
	}

	public Vacation getVacation() {
		return vacation;
	}

	public void setVacation(Vacation vacation) {
		this.vacation = vacation;
	}

	public JobApplication getJobApplication() {
		return jobApplication;
	}

	public void setJobApplication(JobApplication jobApplication) {
		this.jobApplication = jobApplication;
	}

	public ApplicationDao getApplicationDao() {
		return applicationDao;
	}

	public void setApplicationDao(ApplicationDao applicationDao) {
		this.applicationDao = applicationDao;
	}

	public JobApplicationService getApplicationService() {
		return applicationService;
	}

	public void setApplicationService(JobApplicationService applicationService) {
		this.applicationService = applicationService;
	}

	public JobApplicationEnum getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(JobApplicationEnum selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public Date getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}

	public Requisition getRequisition() {
		return requisition;
	}

	public void setRequisition(Requisition requisition) {
		this.requisition = requisition;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Contractor getContractor() {
		return contractor;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}

}